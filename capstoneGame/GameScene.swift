//
//  AppDelegate.swift
//  capstoneGame
//
//  Created by Prabhjinder Singh on 2019-12-02.
//  Copyright © 2019 Prabhjinder. All rights reserved.
//


import SpriteKit
import GameplayKit
import Foundation
import SpriteKit
import GameplayKit
import FirebaseCore
import FirebaseFirestore
import AVFoundation

protocol TransitionDelegate: SKSceneDelegate {
    func returnToMainMenu()
}

class GameScene: SKScene, SKPhysicsContactDelegate{
    let defaults = UserDefaults.standard
  deinit {
       print("\n THE SCENE \((type(of: self))) WAS REMOVED FROM MEMORY (DEINIT) \n")
  }
    
    private var label : SKLabelNode?
    private var spinnyNode : SKShapeNode?
    let background1 = SKSpriteNode(imageNamed: "bg")
    let background2 = SKSpriteNode(imageNamed: "bg")
    let cat = SKSpriteNode(imageNamed: "Run1")
    var bomb = SKSpriteNode(imageNamed: "bomb")
    var stopBackground = false
    var bombDirectionRight = true
    var counterTimer = Timer()
    var counter = 0

    var audioplayer = AVAudioPlayer()
    

  
    
    
    var catdeadpause = false
       var boxArray:[SKSpriteNode] = [SKSpriteNode]()
  //adding the stones
   
    var scoreLabel:SKLabelNode!
    var score = 0
    // lives label
     var livesLabel:SKLabelNode!
     var lives = 5
     
     // jumps label
     var jumpsLabel:SKLabelNode!
     var jumps = 5
    func sound(name: String) {
       do{
           audioplayer = try AVAudioPlayer(contentsOf:URL.init(fileURLWithPath: Bundle.main.path(forResource: name, ofType: "wav")!) )
            audioplayer.prepareToPlay()
       }
        catch{
            print(error)
        }
        audioplayer.play()
    }
    func definingphysics(){
        let catTexture = SKTexture(imageNamed: "Run1.png")
       // self.cat.physicsBody = SKPhysicsBody(texture: catTexture, size: catTexture.size())
             self.cat.physicsBody = SKPhysicsBody(circleOfRadius: self.bomb.size.width/10)
        self.cat.physicsBody?.affectedByGravity = false
        self.cat.physicsBody?.categoryBitMask = 1
        self.cat.physicsBody?.collisionBitMask = 0
        self.cat.physicsBody?.contactTestBitMask = 6
        self.cat.physicsBody?.allowsRotation = false
        self.cat.name = "cat"
        
        
        
    }
    func backgroundPlacement(){
        background1.position = CGPoint(x: frame.size.width / 2, y:frame.size.height / 2)
        background1.size = CGSize(width: frame.width, height: frame.height)
        background1.anchorPoint = CGPoint.zero
        background1.position = CGPoint(x: 0, y: 70)
        background1.zPosition = -15
        self.addChild(background1)

        background2.size = CGSize(width: frame.width, height: frame.height)
        background2.anchorPoint = CGPoint.zero
        background2.position = CGPoint(x: background1.size.width - 1,y: 70)
        background2.zPosition = -15
        self.addChild(background2)
    }
    
    func moveBackgroundLoop(){
        background1.position = CGPoint(x: background1.position.x-4, y: background1.position.y)
        background2.position = CGPoint(x: background2.position.x-4, y: background2.position.y)
        
        for (index, box) in self.boxArray.enumerated() {
            if(box.position.x >= -1000)
            {
                box.position.x = box.position.x-4
            }
   
               }
        if(background1.position.x < -background1.size.width)
        {
            background1.position = CGPoint(x: background1.size.width-9,y: 70)
            self.score += 10

        }
        if(background2.position.x < -background2.size.width)
        {
            background2.position = CGPoint(x: background1.size.width-9,y: 70)
            self.score += 10

        }
    }
    
    func spawnBomb(){
        if (bombDirectionRight == true)
        {
            let throwBombAction = SKAction.applyImpulse(
                CGVector(dx: 40, dy: 150),
                duration: 1)
            self.bomb.run(throwBombAction)

        }
        else{
            let throwBombAction1 = SKAction.applyImpulse(
                CGVector(dx: -80, dy: 150),
                duration: 1)
            self.bomb.run(throwBombAction1)
        }
         
        if(bomb.position.x >= 700)
        {
            bombDirectionRight = false
            
        }
//        else if(bomb.position.x <= 170)
//        {
//            bombDirectionRight = true
//        }
           
    }
    
    override func didMove(to view: SKView) {
        self.physicsWorld.contactDelegate = self
        
        self.scoreLabel = SKLabelNode(text: "Score: \(self.score)")
                  self.scoreLabel.position = CGPoint(x:120, y:590)
                  self.scoreLabel.fontColor = UIColor.magenta
                  self.scoreLabel.fontSize = 55
                  self.scoreLabel.fontName = "Avenir"
                  addChild(self.scoreLabel)
        
        
        // lives label
        self.livesLabel = SKLabelNode(text: "Lives: \(self.lives)")
                  self.livesLabel.position = CGPoint(x:540, y:590)
                  self.livesLabel.fontColor = UIColor.magenta
                  self.livesLabel.fontSize = 55
                  self.livesLabel.fontName = "Avenir"
                  addChild(self.livesLabel)
        
        // jumps label
        self.jumpsLabel = SKLabelNode(text: "Jumps: \(self.jumps)")
                  self.jumpsLabel.position = CGPoint(x:940, y:590)
                  self.jumpsLabel.fontColor = UIColor.magenta
                  self.jumpsLabel.fontSize = 55
                  self.jumpsLabel.fontName = "Avenir"
                 // addChild(self.jumpsLabel)

        
        
        // Get label node from scene and store it for use later
        definingphysics()
        backgroundPlacement()
        cat.position = CGPoint(x:self.size.width*0.24, y:240)
        addChild(cat)
        catAnimation()
       
        dogAnimation()
        startCounter()
        
        
        
    }
    
    func didBegin(_ contact: SKPhysicsContact)
    {
        
        //print("Something collided!")
        let nodeA = contact.bodyA.node
        let nodeB = contact.bodyB.node
        if (nodeA == nil || nodeB == nil)
        {
            return
        }
        if (nodeA!.name == "cat" && nodeB!.name == "bomb" || nodeA!.name == "bomb" && nodeB!.name == "cat")
        {
            lives -= 1
            print("lives remaining: \(lives)")
          
            
            print("A: \(nodeA!.name)  b: \(nodeB!.name)")
            catDeadAnimation()
            bombBlastAnimation()
            stopBackground = true
        }
        else if(nodeA!.name == "cat" && nodeB!.name == "box" || nodeA!.name == "box" && nodeB!.name == "cat")
        {
            lives -= 1
            print("A: \(nodeA!.name)  b: \(nodeB!.name)")
            
            stopBackground = true
            //box.removeFromParent()
            //self.boxArray.remove(at:index)
            
            catDeadAnimation()
            cat.position = CGPoint(x:self.size.width*0.22, y:270)
            if(nodeA!.name == "box" )
            {
                nodeA?.removeFromParent()
                for (index, box) in self.boxArray.enumerated()
                {
                    if(box.position.x == nodeA?.position.x)
                    {
                        box.removeFromParent()
                    }
                }
            }
            else if(nodeB!.name == "box")
            {
                nodeB?.removeFromParent()
                for (index, box) in self.boxArray.enumerated()
                {
                    if(box.position.x == nodeB?.position.x)
                    {
                        box.removeFromParent()
                    }
                }
            }
            
        }
        else if(nodeA!.name == "cat" && nodeB!.name == "power" || nodeA!.name == "power" && nodeB!.name == "cat")
        {
            print("A: \(nodeA!.name)  b: \(nodeB!.name)")
           
        }
        
    }

    
    var boxCount = 0
    func boxfirst(yValue: CGFloat)
    {
        let boxTexture = SKTexture(imageNamed: "box.png")
        let box = SKSpriteNode(imageNamed: "box")
        box.physicsBody = SKPhysicsBody(circleOfRadius: box.size.width/2.2)
            //SKPhysicsBody(texture: boxTexture, size: boxTexture.size())
        box.physicsBody?.affectedByGravity = false
        box.physicsBody?.categoryBitMask = 2
        box.physicsBody?.collisionBitMask = 0
        box.physicsBody?.contactTestBitMask = 1
        box.physicsBody?.allowsRotation = true
        box.name = "box"
        
        box.position = CGPoint(x: size.width+50, y:yValue+70)
         
       // self.boxArray[self.boxArray.count-1].position.x
         box.size = CGSize(width: 160, height: 160)
       //  box.anchorPoint = CGPoint.zero
        box.zPosition = -13
         self.addChild(box)
        self.boxArray.append(box)
                 print(boxArray.count)
        
    }
        func showLabel(){
            // showing the scores
            var currentScoreLabel : SKLabelNode?
             var highestScoreLabel : SKLabelNode?
             var currentScore = 0
            
            let background = SKSpriteNode(imageNamed: "bg")
            background.position = CGPoint(x: frame.size.width / 2, y:frame.size.height / 2)
                              background.size = CGSize(width: frame.width, height: frame.height)
                              background.anchorPoint = CGPoint.zero
                              background.position = CGPoint(x: 0, y: 70)
                              background.zPosition = 2
                              self.addChild(background)
            //score labels
            currentScoreLabel = SKLabelNode(text: "Current Score: \(currentScore)")
               currentScoreLabel!.position = CGPoint(x:300, y:590)
               currentScoreLabel!.fontColor = UIColor.magenta
               currentScoreLabel!.fontSize = 55
               currentScoreLabel!.fontName = "Avenir"
            currentScoreLabel!.zPosition = 3

               addChild(currentScoreLabel!)
               
               highestScoreLabel = SKLabelNode(text: "")
               highestScoreLabel!.position = CGPoint(x:300, y:390)
               highestScoreLabel!.fontColor = UIColor.magenta
               highestScoreLabel!.fontSize = 55
               highestScoreLabel!.fontName = "Avenir"
             highestScoreLabel!.zPosition = 3
               addChild(highestScoreLabel!)
            // firestore
              var db: Firestore!
             var ref = Firestore.firestore().collection("easyLevelScore")
            var a = 0
           
            print("Current score 111... \(currentScore)")
                            
               currentScore = (self.defaults.integer(forKey: "EasyLevelCurrentScore"))
            print("current score \(currentScore)")
                   
                   currentScoreLabel?.text = "Current Score: \(currentScore)"
                   
    //               self.highestScoreLabel?.text = "Highest Score: \(self.highestScore)"
             
            ref.addSnapshotListener{(snapshot, _) in
                                            guard let snapshot = snapshot else {return}
                                            for document in snapshot.documents {
                                                print(" highest score: \(document.data()["HighestScore"] as! Int)")
                                                a = document.data()["HighestScore"] as! Int
                                                
                                                if(currentScore > document.data()["HighestScore"] as! Int){
                                                              
                                                    ref.document("1").setData(["HighestScore": currentScore])

                                                             }

                                                highestScoreLabel?.text = "Highest Score: \(document.data()["HighestScore"] as! Int)"
                                            }
                                                               
                                        }
             print("Highest score 111... \(a)")
        }
    
    func startCounter(){
               counterTimer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(incrementCounter), userInfo: nil, repeats: true)
           }
           @objc func incrementCounter(){
            self.counter = self.counter + 1
            
            if(self.bomb.position.x <= 0){
                bomb.removeFromParent()
                print("bomb disappear position less than 0")
               // makeBomb()
            }
            
            
            self.score = self.score + 1
            self.scoreLabel.text = "Score: \(self.score)"
            self.livesLabel.text = "Lives: \(self.lives)"
            self.jumpsLabel.text = "Jump: \(self.jumps)"
            spawnBomb()
            if(self.counter % 3 == 0 && self.stopBackground == false && self.catdeadpause == false){
            let randomInt = Int.random(in: 1...3)
                
            print("random number \(randomInt)")
                      if(randomInt == 1){
                        boxfirst(yValue: 160)
                      }
                      if(randomInt == 2){
                        boxfirst(yValue: 160)
                         boxfirst(yValue: 235)
                      }
                      if(randomInt == 3){
                        boxfirst(yValue: 160)
                        boxfirst(yValue: 235)
                        boxfirst(yValue: 310)
                      }
            }
            
        }
    
    
    func stopTimer() {
        counterTimer.invalidate()
        //counterTimer = nil
    }
    
    
    
    func catAnimation(){
        if(self.stopBackground == false)
        {
            let image1 = SKTexture(imageNamed: "Run1")
            let image2 = SKTexture(imageNamed: "Run2")
            let image3 = SKTexture(imageNamed: "Run3")
            let image4 = SKTexture(imageNamed: "Run4")
            let image5 = SKTexture(imageNamed: "Run5")
            let image6 = SKTexture(imageNamed: "Run6")
            let image7 = SKTexture(imageNamed: "Run7")
            let image8 = SKTexture(imageNamed: "Run8")
            let punchTextures = [image1,image2,image3,image4,image5,image6,image7,image8]
            let punchAnimation = SKAction.animate(
                           with: punchTextures,
                           timePerFrame: 0.1)
            self.cat.run(punchAnimation)
            let catAnimationForeever = SKAction.repeatForever(punchAnimation)
            self.cat.run(catAnimationForeever)
            cat.zPosition = 1
        }
    }
    func dogAnimation(){
         let image1 = SKTexture(imageNamed: "dogRun1")
         let image2 = SKTexture(imageNamed: "dogRun2")
         let image3 = SKTexture(imageNamed: "dogRun3")
         let image4 = SKTexture(imageNamed: "dogRun4")
         let image5 = SKTexture(imageNamed: "dogRun5")
         let image6 = SKTexture(imageNamed: "dogRun6")
         let image7 = SKTexture(imageNamed: "dogRun7")
         let image8 = SKTexture(imageNamed: "dogRun8")
         let punchTextures = [image1,image2,image3,image4,image5,image6,image7,image8]
         let punchAnimation = SKAction.animate(
                        with: punchTextures,
                        timePerFrame: 0.1)
    
        
    }
    func catJumpAnimation(){
        sound(name: "jump")
        let image1 = SKTexture(imageNamed: "Jump1")
        let image2 = SKTexture(imageNamed: "Jump4")
     //   let image3 = SKTexture(imageNamed: "Jump3")
        let image4 = SKTexture(imageNamed: "Jump2")
        let punchTextures = [image1,image2,image4]
        let punchAnimation = SKAction.animate(
                       with: punchTextures,
                       timePerFrame: 0.6)
        self.cat.run(punchAnimation)
        cat.zPosition = 1
        
    }
      func catDeadAnimation(){
        sound(name: "death")
        catdeadpause = true
            let image1 = SKTexture(imageNamed: "catdead1")
            let image2 = SKTexture(imageNamed: "catdead2")
            let image3 = SKTexture(imageNamed: "catdead3")
            let image4 = SKTexture(imageNamed: "catdead4")
            let punchTextures = [image1,image2,image3,image4]
            let punchAnimation = SKAction.animate(
                           with: punchTextures,
                           timePerFrame: 0.2)
        self.cat.run(punchAnimation, completion:{ self.catdeadpause = false})
            cat.zPosition = 1
            
        }
        func bombBlastAnimation(){
            sound(name: "bombExplosion")
              let image1 = SKTexture(imageNamed: "pow")
                         let punchTextures = [image1]
                         let punchAnimation = SKAction.animate(
                                        with: punchTextures,
                                        timePerFrame: 0.2)
            self.bomb.run(punchAnimation, completion: {
                self.bomb.removeFromParent()
               // self.makeBomb()
            })
              
          }
    func touchDown(atPoint pos : CGPoint) {
    }
    
    func touchMoved(toPoint pos : CGPoint) {
    }
    
    func touchUp(atPoint pos : CGPoint) {
        if let n = self.spinnyNode?.copy() as! SKShapeNode? {
            n.position = pos
            n.strokeColor = SKColor.red
            self.addChild(n)
        }
    }
    var counterJump = 0;
    func jumpingcatdog()  {
        if(cat.position.y <= 450)
        {
            print("jump")
            self.jumps -= 1
            print(jumps)
            catJumpAnimation()
            let jumpUpAction = SKAction.moveBy(x: 0, y:150, duration:0.9)
            let jumpDownAction = SKAction.moveBy(x: 0, y:-150, duration:0.9 )
            let jumpSequence = SKAction.sequence([jumpUpAction, jumpDownAction])
            print("y position: \(cat.position.y)")
            cat.run(jumpSequence)
        }
        
        
    }
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
       // let location = (touches.first as! UITouch).location(in: self.view)
        // stone tapped
        
          
              guard let mousePosition = touches.first?.location(in: self) else {
                    return
                }
            
            if(catdeadpause == false)
            {
                if (mousePosition.y > (self.size.height)/2){
                jumpingcatdog()
                    
            }
            else{
            if mousePosition.x < (self.size.width)/2 {
                print("left")
                stopBackground = true
                if(self.cat.position.x >= 100)
                {
                    self.cat.position.x -= 15;
                    print("\(self.cat.position.x)");

                }
               // MoveLeft()
            } else {
                // right code
                
                   print("right")
                if(self.cat.position.x <= (self.size.width)*0.25)
                             {
                                 stopBackground = true
                             }
                else
                {
                     stopBackground = false
                }
                if(self.cat.position.x <= (self.size.width)/1.5)
                {
                    self.cat.position.x += 15;
                    print("\(self.cat.position.x)");
                }

            }}
        }
        
      
    }
    
    
    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
        for t in touches { self.touchMoved(toPoint: t.location(in: self)) }
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        for t in touches { self.touchUp(atPoint: t.location(in: self)) }
    }
    
    override func touchesCancelled(_ touches: Set<UITouch>, with event: UIEvent?) {
        for t in touches { self.touchUp(atPoint: t.location(in: self)) }
    }
    
    
    
    override func update(_ currentTime: TimeInterval) {
      
      
        
//        if(self.stone1Label.text == "0"){
//        self.stone1.removeFromParent()
//        }
        // Called before each frame is rendered
        if(stopBackground == false)
        {
            moveBackgroundLoop()
        }
                if(lives == 0){
                    
     self.stopTimer()
                     self.defaults.set("\(self.score)", forKey: "EasyLevelCurrentScore")
                
                    
                    showLabel()
                                        self.run(SKAction.wait(forDuration: 1),completion:{[unowned self] in
                                                       guard let delegate = self.delegate else { return }
                                                       self.view?.presentScene(nil)
                                                       (delegate as! TransitionDelegate).returnToMainMenu()
                                            self.removeAllActions()
                                            self.removeAllChildren()
                                            
                                                   })
//                    let mainGameScreen = EasyGameScore(size:self.size)
//                           self.view?.presentScene(mainGameScreen)
                    
                    
                                    }
        if(jumps == 0){
//                      self.stopTimer()
//
//            self.defaults.set("\(self.score)", forKey: "EasyLevelCurrentScore")
//            showLabel()
//                                      self.run(SKAction.wait(forDuration: 1),completion:{[unowned self] in
//                                                     guard let delegate = self.delegate else { return }
//                                                     self.view?.presentScene(nil)
//                                                     (delegate as! TransitionDelegate).returnToMainMenu()
//
//                                         self.removeAllActions()
//                                                                                   self.removeAllChildren()
//                                                 })
//            let mainGameScreen = EasyGameScore(size:self.size)
//            self.view?.presentScene(mainGameScreen)
           
                                  }

    func powerUp(stone: SKSpriteNode) {
        var powerNode = SKSpriteNode(imageNamed: "power")

        powerNode.anchorPoint = CGPoint(x: 0.5,y: 0.5)
              powerNode.position.x = stone.position.x
              powerNode.position.y = stone.position.y
              powerNode.size = CGSize(width: 67, height: 60)
              powerNode.texture = SKTexture(imageNamed: "power.png")
             
              powerNode.zPosition = 998
        powerNode.name = "power"

        
          
                  powerNode.physicsBody = SKPhysicsBody(rectangleOf: powerNode.size)
                  powerNode.physicsBody?.affectedByGravity = false
                  powerNode.physicsBody?.categoryBitMask = 8
                  powerNode.physicsBody?.collisionBitMask = 0
                  powerNode.physicsBody?.contactTestBitMask = 1
                  powerNode.physicsBody?.allowsRotation = true
             
              
              addChild(powerNode)
       
              
              let move1 = SKAction.move(to: CGPoint(x: stone.position.x , y: 200),
                                                duration: 7)
                
                      let stoneAnimation = SKAction.sequence(
                          [move1]
                      )
                    
        powerNode.run(stoneAnimation, completion: {
            powerNode.removeFromParent()
        })
        
    }
        
}
}
