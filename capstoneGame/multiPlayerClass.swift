//
//  multiPlayerClass.swift
//  capstoneGame
//
//  Created by Prabhjinder Singh on 2019-12-06.
//  Copyright © 2019 Prabhjinder. All rights reserved.
//

import Foundation
import UIKit
import SpriteKit
import GameplayKit
import FirebaseCore
import FirebaseFirestore

class multiPlayerClass: UIViewController {
  var roomCreated = false
   var roomJoined = false
   var a = false,b = false
    
    @IBOutlet weak var dogButton: UIButton!
    @IBOutlet weak var catButton: UIButton!
    // User defaults
       let defaults = UserDefaults.standard
    var counterTimer = Timer()
    var counter = 0

    @IBOutlet weak var roomLabel: UILabel!
    @IBOutlet weak var joinLabel: UILabel!
    @IBOutlet weak var playGame: UIButton!
     @IBOutlet weak var multiPlayerButton: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        joinLabel.isHidden = true
        roomLabel.isHidden = true
        // imageView.image = UIImage(named: "bg")
       // imageView.se
      //  imageView.frame.size = CGSize(width: screensize.width, height: <#T##CGFloat#>)
         
           // FirebaseApp.configure()
        
        self.multiPlayerButton.isHidden = true
        
        // firestore
                            var db: Firestore!
                           var ref = Firestore.firestore().collection("roomCreated")
                            ref.document("P2YQ9AZkArm5CRse78KC").setData(["roomCreated1": false])
        
       var ref2 = Firestore.firestore().collection("roomJoined")
        ref2.document("Gbxnk6Eq39ZQUeQwYyIa").setData(["roomJoined1": false])
        startCounter()
        
    }
    @IBAction func createRoomButton(_ sender: Any) {
        self.dogButton.isHidden = true
        
       // var db: Firestore!
            var ref1 = Firestore.firestore().collection("roomCreated")
       // update
        ref1.document("P2YQ9AZkArm5CRse78KC").setData(["roomCreated1": true])
        ref1.addSnapshotListener{(snapshot, _) in
            guard let snapshot = snapshot else {return}
            for document in snapshot.documents {
                print(document.data()["roomCreated1"]!)

                var a = document.data()["roomCreated1"]! as! Bool
                print("a ....\(a)")
                if( a == true){
                    self.roomLabel.text = "Room Created"

                    self.defaults.set("cat", forKey: "Player")
                    print("cat cat cat  cat")
                    print("player name \(self.defaults.string(forKey: "Player")!)")
                }
            }
        }
        
        

    }

        @IBAction func joinRoomButton(_ sender: Any) {
            self.catButton.isHidden = true
            self.defaults.set("dog", forKey: "Player")
                                                               print("dog dog dog dog")
    //        var db: Firestore!
            var ref = Firestore.firestore().collection("roomCreated")
            //update
            ref.addSnapshotListener{(snapshot, _) in
                  guard let snapshot = snapshot else {return}
                  for document in snapshot.documents {
                      print(document.data()["roomCreated1"]!)



                      var a = document.data()["roomCreated1"]! as! Bool
                    ref = Firestore.firestore().collection("roomJoined")
                      print("a ....\(a)")
                      if( a == true){
                            ref.document("Gbxnk6Eq39ZQUeQwYyIa").setData(["roomJoined1": true])
                                ref.addSnapshotListener{(snapshot, _) in
                                    guard let snapshot = snapshot else {return}
                                    for document in snapshot.documents {
                                        print(document.data()["roomJoined1"]!)
                                            var b = document.data()["roomJoined1"]! as! Bool
                                                   print("a ....\(a)")
                                                   if( b == true){
                                                       self.joinLabel.text = "Room Joined"


                                                   }
                                    }
                                }
                      }
                      else{
                        self.joinLabel.text = "Room not Joined"
                    }
                  }
              }

            
            
          

        }
    
    func startCounter(){
               counterTimer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(incrementCounter), userInfo: nil, repeats: true)
           }
           @objc func incrementCounter(){
            

            self.counter = self.counter + 1
            var ref = Firestore.firestore().collection("roomCreated")
            ref.addSnapshotListener{(snapshot, _) in
                     guard let snapshot = snapshot else {return}
                     for document in snapshot.documents {
                     //    print(document.data()["roomCreated1"]!)

                        self.a = document.data()["roomCreated1"]! as! Bool
                        print("a ....\(self.a)")
                        if( self.a == true){
                             self.roomLabel.text = "Room Created"
                            self.catButton.isHidden = true
                         }
                     }
                 }


            ref = Firestore.firestore().collection("roomJoined")
                      ref.addSnapshotListener{(snapshot, _) in
                               guard let snapshot = snapshot else {return}
                               for document in snapshot.documents {
                               //    print(document.data()["roomJoined1"]!)

                                self.b = document.data()["roomJoined1"]! as! Bool
                                print("b ....\(self.b)")
                                if( self.b == true){
                                      self.joinLabel.text = "Room Joined"
                                    self.dogButton.isHidden = true
                                   }
                               }
                           }
            if( self.a == true && self.b == true){
                print("Multiplayer button appeared")
                let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                let newViewController = storyBoard.instantiateViewController(withIdentifier: "multiGame") as! MultiGameViewController
                        self.present(newViewController, animated: true, completion: nil)
              //  self.multiPlayerButton.isHidden = false

            }
                 
    }



}
