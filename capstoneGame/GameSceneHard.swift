//
//  GameSceneHard.swift
//  capstoneGame
//
//  Created by Satinder pal Singh on 2019-12-06.
//  Copyright © 2019 Prabhjinder. All rights reserved.
//

import Foundation
import SpriteKit
import GameplayKit
import AVFoundation
import FirebaseCore
import FirebaseFirestore

protocol HardTransitionDelegate: SKSceneDelegate {
    func returnToMainMenu()
}

class GameSceneHard: SKScene, SKPhysicsContactDelegate {
 deinit {
           print("\n THE SCENE \((type(of: self))) WAS REMOVED FROM MEMORY (DEINIT) \n")
      }
        
    let defaults = UserDefaults.standard
        private var label : SKLabelNode?
        private var spinnyNode : SKShapeNode?
        let background1 = SKSpriteNode(imageNamed: "bg")
        let background2 = SKSpriteNode(imageNamed: "bg")
        let cat = SKSpriteNode(imageNamed: "Run1")
       // let dog = SKSpriteNode(imageNamed: "dogRun1")
        var bomb = SKSpriteNode(imageNamed: "bomb")
        var audioplayer = AVAudioPlayer()

            var stopBackground = false
        var bombDirectionRight = true
        var counterTimer = Timer()
           var counter = 0
        var powerArray:[SKSpriteNode] = [SKSpriteNode]()
           var boxArray:[SKSpriteNode] = [SKSpriteNode]()
           var stoneArray:[SKSpriteNode] = [SKSpriteNode]()
         
        var catdeadpause = false
        //adding the stones
        let stone1 = SKSpriteNode(imageNamed: "stone")
           let stone2 = SKSpriteNode(imageNamed: "stone")
           let stone3 = SKSpriteNode(imageNamed: "stone")
           let stone4 = SKSpriteNode(imageNamed: "stone")
           let stone5 = SKSpriteNode(imageNamed: "stone")
        
        var numStone1 = 10
        var numStone2 = 10
        var numStone3 = 10
        var numStone4 = 10
        var numStone5 = 10
        
        var stone1Label:SKLabelNode!
        var stone2Label:SKLabelNode!
        var stone3Label:SKLabelNode!
        var stone4Label:SKLabelNode!
        var stone5Label:SKLabelNode!
        
        var scoreLabel:SKLabelNode!
        var score = 0
        // lives label
         var livesLabel:SKLabelNode!
         var lives = 5
         
         // jumps label
         var jumpsLabel:SKLabelNode!
         var jumps = 50
          func sound(name: String) {
           do{
               audioplayer = try AVAudioPlayer(contentsOf:URL.init(fileURLWithPath: Bundle.main.path(forResource: name, ofType: "wav")!) )
                audioplayer.prepareToPlay()
           }
            catch{
                print(error)
            }
            audioplayer.play()
        }
        func definingphysics(){
            let catTexture = SKTexture(imageNamed: "Run1.png")
           // self.cat.physicsBody = SKPhysicsBody(texture: catTexture, size: catTexture.size())
                 self.cat.physicsBody = SKPhysicsBody(circleOfRadius: self.bomb.size.width/10)
            self.cat.physicsBody?.affectedByGravity = false
            self.cat.physicsBody?.categoryBitMask = 1
            self.cat.physicsBody?.collisionBitMask = 0
            self.cat.physicsBody?.contactTestBitMask = 6
            self.cat.physicsBody?.allowsRotation = false
            self.cat.name = "cat"
            
            
            
        }
        func backgroundPlacement(){
            background1.position = CGPoint(x: frame.size.width / 2, y:frame.size.height / 2)
            background1.size = CGSize(width: frame.width, height: frame.height)
            background1.anchorPoint = CGPoint.zero
            background1.position = CGPoint(x: 0, y: 70)
            background1.zPosition = -15
            self.addChild(background1)

            background2.size = CGSize(width: frame.width, height: frame.height)
            background2.anchorPoint = CGPoint.zero
            background2.position = CGPoint(x: background1.size.width - 1,y: 70)
            background2.zPosition = -15
            self.addChild(background2)
        }
        
        func moveBackgroundLoop(){
            background1.position = CGPoint(x: background1.position.x-4, y: background1.position.y)
            background2.position = CGPoint(x: background2.position.x-4, y: background2.position.y)
            
            for (index, box) in self.boxArray.enumerated() {
                if(box.position.x >= -100)
                {
                    box.position.x = box.position.x-4
                }
                   }
            if(background1.position.x < -background1.size.width)
            {
                self.score += 10
                background1.position = CGPoint(x: background1.size.width-9,y: 70)
            }
            if(background2.position.x < -background2.size.width)
            {
                self.score += 10
                background2.position = CGPoint(x: background1.size.width-9,y: 70)
            }
        }
        
        func spawnBomb(){
            if (bombDirectionRight == true)
            {
                let throwBombAction = SKAction.applyImpulse(
                    CGVector(dx: 40, dy: 150),
                    duration: 1)
                self.bomb.run(throwBombAction)

            }
            else{
                let throwBombAction1 = SKAction.applyImpulse(
                    CGVector(dx: -80, dy: 150),
                    duration: 1)
                self.bomb.run(throwBombAction1)
            }
             
            if(bomb.position.x >= 700)
            {
                bombDirectionRight = false
                
            }
    //        else if(bomb.position.x <= 170)
    //        {
    //            bombDirectionRight = true
    //        }powee
               
        }
        
        override func didMove(to view: SKView) {
            self.physicsWorld.contactDelegate = self
            
            self.scoreLabel = SKLabelNode(text: "Score: \(self.score)")
                      self.scoreLabel.position = CGPoint(x:120, y:590)
                      self.scoreLabel.fontColor = UIColor.magenta
                      self.scoreLabel.fontSize = 55
                      self.scoreLabel.fontName = "Avenir"
                      addChild(self.scoreLabel)
            
            
            // lives label
            self.livesLabel = SKLabelNode(text: "Lives: \(self.lives)")
                      self.livesLabel.position = CGPoint(x:540, y:590)
                      self.livesLabel.fontColor = UIColor.magenta
                      self.livesLabel.fontSize = 55
                      self.livesLabel.fontName = "Avenir"
                      addChild(self.livesLabel)
            
            // jumps label
            self.jumpsLabel = SKLabelNode(text: "Jumps: \(self.jumps)")
                      self.jumpsLabel.position = CGPoint(x:940, y:590)
                      self.jumpsLabel.fontColor = UIColor.magenta
                      self.jumpsLabel.fontSize = 55
                      self.jumpsLabel.fontName = "Avenir"
                      addChild(self.jumpsLabel)
            
            
            // Get label node from scene and store it for use later
            definingphysics()
            backgroundPlacement()
            cat.position = CGPoint(x:self.size.width*0.25, y:240)
            addChild(cat)
            catAnimation()
           
           
            startCounter()
            makeStone()
            makeBomb()
            
        }
        
        func didBegin(_ contact: SKPhysicsContact)
        {
            
            //print("Something collided!")
            let nodeA = contact.bodyA.node
            let nodeB = contact.bodyB.node
            if (nodeA == nil || nodeB == nil)
            {
                return
            }
            if (nodeA!.name == "cat" && nodeB!.name == "bomb" || nodeA!.name == "bomb" && nodeB!.name == "cat")
            {
                lives -= 1
                print("lives remaining: \(lives)")
                print("A: \(nodeA!.name)  b: \(nodeB!.name)")
                catDeadAnimation()
                bombBlastAnimation()
                stopBackground = true
            }
            else if(nodeA!.name == "cat" && nodeB!.name == "box" || nodeA!.name == "box" && nodeB!.name == "cat")
            {
                lives -= 1
                print("A: \(nodeA!.name)  b: \(nodeB!.name)")
                
                stopBackground = true
                //box.removeFromParent()
                //self.boxArray.remove(at:index)
                
                catDeadAnimation()
                cat.position = CGPoint(x:self.size.width*0.22, y:270)
                if(nodeA!.name == "box" )
                {
                    nodeA?.removeFromParent()
                    for (index, box) in self.boxArray.enumerated()
                    {
                        if(box.position.x == nodeA?.position.x)
                        {
                            box.removeFromParent()
                        }
                    }
                }
                else if(nodeB!.name == "box")
                {
                    nodeB?.removeFromParent()
                    for (index, box) in self.boxArray.enumerated()
                    {
                        if(box.position.x == nodeB?.position.x)
                        {
                            box.removeFromParent()
                        }
                    }
                }
                
            }
            else if(nodeA!.name == "cat" && nodeB!.name == "power" || nodeA!.name == "power" && nodeB!.name == "cat")
            {
                print("A: \(nodeA!.name)  b: \(nodeB!.name)")
                //self.jumps += 5
               // powerNode.removeFromParent()
            }
            
   

        }
        func makeBomb(){
            self.bomb = SKSpriteNode(imageNamed: "bomb")
            // self.bomb.anchorPoint = CGPoint.zero
            self.bombDirectionRight = true
                            self.bomb.position.x = -50
                            self.bomb.position.y = 590
                            self.bomb.size = CGSize(width: 100, height: 95)
                            self.bomb.texture = SKTexture(imageNamed: "bomb.png")
            bomb.name = "bomb"
                            self.bomb.zPosition = 999
                  
                  self.bomb.physicsBody = SKPhysicsBody(circleOfRadius: self.bomb.size.width/3)
                       self.bomb.physicsBody?.affectedByGravity = true
                       self.bomb.physicsBody?.isDynamic = true
                       self.bomb.physicsBody?.categoryBitMask = 4
                       self.bomb.physicsBody?.restitution = 1
                       self.bomb.physicsBody?.collisionBitMask = 16
                       self.bomb.physicsBody?.contactTestBitMask = 9
                       self.bomb.physicsBody?.allowsRotation = true
            addChild(self.bomb)
            spawnBomb()
        }
        func makeStone(){
            stone1.position = CGPoint(x: 143.5, y:110)
            stone1.size = CGSize(width: 67, height: 60)
            stone1.anchorPoint = CGPoint(x: 0.5,y: 0.5)
            stone1.zPosition = -13
            self.addChild(stone1)
            
            
            stone2.position = CGPoint(x: 403.5, y:110)
            stone2.size = CGSize(width: 67, height: 60)
            stone2.anchorPoint = CGPoint(x: 0.5,y: 0.5)
            stone2.zPosition = -13
            self.addChild(stone2)
            
            stone3.position = CGPoint(x: 670.5, y:110)
            stone3.size = CGSize(width: 67, height: 60)
            stone3.anchorPoint = CGPoint(x: 0.5,y: 0.5)
            stone3.zPosition = -13
            self.addChild(stone3)
            
            stone4.position = CGPoint(x: 937.5, y:110)
            stone4.size = CGSize(width: 67, height: 60)
            stone4.anchorPoint = CGPoint(x: 0.5,y: 0.5)
            stone4.zPosition = -13
            self.addChild(stone4)
            
            stone5.position = CGPoint(x: 1204.5, y:110)
            stone5.size = CGSize(width: 67, height: 60)
            stone5.anchorPoint = CGPoint(x: 0.5,y: 0.5)
            stone5.zPosition = -13
            self.addChild(stone5)
            
            
            // adding number label on stones
            self.stone1Label = SKLabelNode(text: "\(self.numStone1)")
                   self.stone1Label.position = CGPoint(x:120, y:90)
                   self.stone1Label.fontColor = UIColor.magenta
                   self.stone1Label.fontSize = 55
                   self.stone1Label.fontName = "Avenir"
                   addChild(self.stone1Label)
            
            self.stone2Label = SKLabelNode(text: "\(self.numStone2)")
            self.stone2Label.position = CGPoint(x:380, y:90)
            self.stone2Label.fontColor = UIColor.magenta
            self.stone2Label.fontSize = 55
            self.stone2Label.fontName = "Avenir"
            addChild(self.stone2Label)
            
            self.stone3Label = SKLabelNode(text: "\(self.numStone3)")
            self.stone3Label.position = CGPoint(x:647, y:90)
            self.stone3Label.fontColor = UIColor.magenta
            self.stone3Label.fontSize = 55
            self.stone3Label.fontName = "Avenir"
            addChild(self.stone3Label)
            
            self.stone4Label = SKLabelNode(text: "\(self.numStone4)")
            self.stone4Label.position = CGPoint(x:914, y:90)
            self.stone4Label.fontColor = UIColor.magenta
            self.stone4Label.fontSize = 55
            self.stone4Label.fontName = "Avenir"
            addChild(self.stone4Label)
            
            self.stone5Label = SKLabelNode(text: "\(self.numStone5)")
            self.stone5Label.position = CGPoint(x:1181, y:90)
            self.stone5Label.fontColor = UIColor.magenta
            self.stone5Label.fontSize = 55
            self.stone5Label.fontName = "Avenir"
            addChild(self.stone5Label)
            
        }
        
        var boxCount = 0
        func boxfirst(yValue: CGFloat)
        {
            let boxTexture = SKTexture(imageNamed: "box.png")
            let box = SKSpriteNode(imageNamed: "box")
            box.physicsBody = SKPhysicsBody(circleOfRadius: box.size.width/2.2)
                //SKPhysicsBody(texture: boxTexture, size: boxTexture.size())
            box.physicsBody?.affectedByGravity = false
            box.physicsBody?.categoryBitMask = 2
            box.physicsBody?.collisionBitMask = 0
            box.physicsBody?.contactTestBitMask = 1
            box.physicsBody?.allowsRotation = true
            box.name = "box"
            
            box.position = CGPoint(x: size.width+50, y:yValue+70)
             
           // self.boxArray[self.boxArray.count-1].position.x
             box.size = CGSize(width: 160, height: 160)
           //  box.anchorPoint = CGPoint.zero
            box.zPosition = -13
             self.addChild(box)
            self.boxArray.append(box)
                     print(boxArray.count)
            
        }
        
        func startCounter(){
                   counterTimer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(incrementCounter), userInfo: nil, repeats: true)
               }
               @objc func incrementCounter(){
                self.counter = self.counter + 1
                
                if(self.bomb.position.x <= 0){
                    bomb.removeFromParent()
                    print("bomb disappear position less than 0")
                    makeBomb()
                }
                
                
                self.score = self.score + 1
                self.scoreLabel.text = "Score: \(self.score)"
                self.livesLabel.text = "Lives: \(self.lives)"
                self.jumpsLabel.text = "Jump: \(self.jumps)"
                spawnBomb()
                if(self.counter % 3 == 0 && self.stopBackground == false && self.catdeadpause == false){
                let randomInt = Int.random(in: 1...3)
                    
                print("random number \(randomInt)")
                          if(randomInt == 1){
                            boxfirst(yValue: 160)
                          }
                          if(randomInt == 2){
                            boxfirst(yValue: 160)
                             boxfirst(yValue: 235)
                          }
                          if(randomInt == 3){
                            boxfirst(yValue: 160)
                            boxfirst(yValue: 235)
                            boxfirst(yValue: 310)
                          }
                }
                
            }
        
        
        func stopTimer() {
            counterTimer.invalidate()
            //counterTimer = nil
        }
        
        
        
        func catAnimation(){
            if(self.stopBackground == false)
            {
                let image1 = SKTexture(imageNamed: "Run1")
                let image2 = SKTexture(imageNamed: "Run2")
                let image3 = SKTexture(imageNamed: "Run3")
                let image4 = SKTexture(imageNamed: "Run4")
                let image5 = SKTexture(imageNamed: "Run5")
                let image6 = SKTexture(imageNamed: "Run6")
                let image7 = SKTexture(imageNamed: "Run7")
                let image8 = SKTexture(imageNamed: "Run8")
                let punchTextures = [image1,image2,image3,image4,image5,image6,image7,image8]
                let punchAnimation = SKAction.animate(
                               with: punchTextures,
                               timePerFrame: 0.1)
                self.cat.run(punchAnimation)
                let catAnimationForeever = SKAction.repeatForever(punchAnimation)
                self.cat.run(catAnimationForeever)
                cat.zPosition = 1
            }
        }
       
        func catJumpAnimation(){
            sound(name: "jump")
            let image1 = SKTexture(imageNamed: "Jump1")
            let image2 = SKTexture(imageNamed: "Jump4")
         //   let image3 = SKTexture(imageNamed: "Jump3")
            let image4 = SKTexture(imageNamed: "Jump2")
            let punchTextures = [image1,image2,image4]
            let punchAnimation = SKAction.animate(
                           with: punchTextures,
                           timePerFrame: 0.6)
            self.cat.run(punchAnimation)
            cat.zPosition = 1
            
        }
          func catDeadAnimation(){
            sound(name: "death")
                catdeadpause = true
                let image1 = SKTexture(imageNamed: "catdead1")
                let image2 = SKTexture(imageNamed: "catdead2")
                let image3 = SKTexture(imageNamed: "catdead3")
                let image4 = SKTexture(imageNamed: "catdead4")
                let punchTextures = [image1,image2,image3,image4]
                let punchAnimation = SKAction.animate(
                               with: punchTextures,
                               timePerFrame: 0.2)
                self.cat.run(punchAnimation, completion:{ self.catdeadpause = false})
                cat.zPosition = 1
                
            }
            func bombBlastAnimation(){
                sound(name: "bombExplosion")
                  let image1 = SKTexture(imageNamed: "pow")
                             let punchTextures = [image1]
                             let punchAnimation = SKAction.animate(
                                            with: punchTextures,
                                            timePerFrame: 0.2)
                self.bomb.run(punchAnimation, completion: {
                    self.bomb.removeFromParent()
                    self.makeBomb()
                })
                  
              }
        func touchDown(atPoint pos : CGPoint) {
        }
        
        func touchMoved(toPoint pos : CGPoint) {
        }
        
        func touchUp(atPoint pos : CGPoint) {
            if let n = self.spinnyNode?.copy() as! SKShapeNode? {
                n.position = pos
                n.strokeColor = SKColor.red
                self.addChild(n)
            }
        }
        var counterJump = 0;
        func jumpingcatdog()  {
            if(cat.position.y <= 450)
            {
                print("jump")
                self.jumps -= 1
                print(jumps)
                catJumpAnimation()
                let jumpUpAction = SKAction.moveBy(x: 0, y:150, duration:0.9)
                let jumpDownAction = SKAction.moveBy(x: 0, y:-150, duration:0.9 )
                let jumpSequence = SKAction.sequence([jumpUpAction, jumpDownAction])
                print("y position: \(cat.position.y)")
                cat.run(jumpSequence)
            }
            
            
        }
        override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
           // let location = (touches.first as! UITouch).location(in: self.view)
            // stone tapped
            
              
                  guard let mousePosition = touches.first?.location(in: self) else {
                        return
                    }
            if(catdeadpause == false)
            {
                if(Int(self.stone1Label.text!)! > 0){
                    if self.stone1.contains(mousePosition){
                        self.numStone1 = self.numStone1 - 1
                        self.stone1Label.text = "\(self.numStone1)"
                        spawnStone(stone: self.stone1)
                    }
                }
                if(Int(self.stone2Label.text!)! > 0){
                     if self.stone2.contains(mousePosition){
                        self.numStone2 = self.numStone2 - 1
                        self.stone2Label.text = "\(self.numStone2)"
                        spawnStone(stone: self.stone2)
                    }
                }
                if(Int(self.stone3Label.text!)! > 0){
                     if self.stone3.contains(mousePosition){
                        self.numStone3 = self.numStone3 - 1
                        self.stone3Label.text = "\(self.numStone3)"
                        spawnStone(stone: self.stone3)
                    }
                }
                if(Int(self.stone4Label.text!)! > 0){
                     if self.stone4.contains(mousePosition){
                        self.numStone4 = self.numStone4 - 1
                        self.stone4Label.text = "\(self.numStone4)"
                        spawnStone(stone: self.stone4)
                    }
                }
                if(Int(self.stone5Label.text!)! > 0){
                     if self.stone5.contains(mousePosition){
                        self.numStone5 = self.numStone5 - 1
                        self.stone5Label.text = "\(self.numStone5)"
                        spawnStone(stone: self.stone5)
                    }
                }
                if (mousePosition.y > (self.size.height)/2){
                    jumpingcatdog()
                        
                }
                else{
                if mousePosition.x < (self.size.width)/2 {
                    print("left")
                    stopBackground = true
                    if(self.cat.position.x >= 100)
                    {
                        self.cat.position.x -= 15;
                        print("\(self.cat.position.x)");

                    }
                   // MoveLeft()
                } else {
                    // right code
                    
                       print("right")
                    if(self.cat.position.x <= (self.size.width)*0.25)
                                 {
                                     stopBackground = true
                                 }
                    else
                    {
                         stopBackground = false
                    }
                    if(self.cat.position.x <= (self.size.width)/1.5)
                    {
                        self.cat.position.x += 15;
                        print("\(self.cat.position.x)");
                    }

                }
            }
            
        }// cat dead pause
        }
        func spawnStone(stone : SKSpriteNode) {
               
            //adding the stone
               let stoneX = SKSpriteNode(imageNamed: "stone")
            stoneX.anchorPoint = CGPoint(x: 0.5,y: 0.5)
            stoneX.position.x = stone.position.x
            stoneX.position.y = stone.position.y
            stoneX.size = CGSize(width: 67, height: 60)
            stoneX.texture = SKTexture(imageNamed: "stone.png")
           
            stoneX.zPosition = 999

      
        
                stoneX.physicsBody = SKPhysicsBody(rectangleOf: stoneX.size)
                stoneX.physicsBody?.affectedByGravity = false
                stoneX.physicsBody?.categoryBitMask = 8
                stoneX.physicsBody?.collisionBitMask = 0
                stoneX.physicsBody?.contactTestBitMask = 4
                stoneX.physicsBody?.allowsRotation = true
           
            
            addChild(stoneX)
            self.stoneArray.append(stoneX)
            
               
            let move1 = SKAction.move(to: CGPoint(x: stone.position.x , y: size.height),
                                              duration: 2)
              
                    let stoneAnimation = SKAction.sequence(
                        [move1]
                    )
                  
                    stoneX.run(stoneAnimation)
               
               
           }
        
        override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
            for t in touches { self.touchMoved(toPoint: t.location(in: self)) }
        }
        
        override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
            for t in touches { self.touchUp(atPoint: t.location(in: self)) }
        }
        
        override func touchesCancelled(_ touches: Set<UITouch>, with event: UIEvent?) {
            for t in touches { self.touchUp(atPoint: t.location(in: self)) }
        }
        
        
        override func update(_ currentTime: TimeInterval) {
          
          
            
    //        if(self.stone1Label.text == "0"){
    //        self.stone1.removeFromParent()
    //        }
            // Called before each frame is rendered
            if(stopBackground == false)
            {
                moveBackgroundLoop()
            }
   
                         for (index, stone) in self.stoneArray.enumerated()
                         {
                            if (self.bomb.frame.intersects(stone.frame) == true)
                                 {
                                    
                                    print("stone array size: \(self.stoneArray.count)")
                                    
                                    powerUp(stone: stone)
                                                           stone.removeFromParent()
                                    if(self.stoneArray.count > index){
                                            self.stoneArray.remove(at:index)
                                    }
                                    else{
                                        print("jugaad")
                                    }
                       
                                    print("stone array size1: \(self.stoneArray.count)")
                                    bombBlastAnimation()
                                    print("collision between stone and bomb")
                                }
                        }
            
            for (index, power) in self.powerArray.enumerated()
              {
                 if (self.cat.frame.intersects(power.frame) == true)
                      {
                         
                         print("stone array size: \(self.stoneArray.count)")
                         
                        
                         if(self.powerArray.count > index){
                            power.removeFromParent()

                                 self.powerArray.remove(at:index)
                            self.jumps += 5
                         }
                         else{
                            //power.removeFromParent()
                             print("jugaad")
                         }
            
                         print("stone array size1: \(self.powerArray.count)")
                       //  bombBlastAnimation()
                         print("collision between stone and bomb")
                     }
             }
            
                    if(lives == 0){
                             self.stopTimer()
                         self.defaults.set("\(self.score)", forKey: "HardLevelCurrentScore")
                        showLabel()
                        
                                            self.run(SKAction.wait(forDuration: 1),completion:{[unowned self] in
                                                           guard let delegate = self.delegate else { return }
                                                           self.view?.presentScene(nil)
                                                           (delegate as! HardTransitionDelegate).returnToMainMenu()
                                                self.removeAllActions()
                                                                                           self.removeAllChildren()
                                                       })
//                        let mainGameScreen = HardGameScore(size:self.size)
//                                       self.view?.presentScene(mainGameScreen)
                                        }
            if(jumps == 0){
                 self.stopTimer()
                self.defaults.set("\(self.score)", forKey: "HardLevelCurrentScore")
                showLabel()
             

                                          self.run(SKAction.wait(forDuration: 1),completion:{[unowned self] in
                                                         guard let delegate = self.delegate else { return }
                                                         self.view?.presentScene(nil)
                                                         (delegate as! TransitionDelegate).returnToMainMenu()
                                                     self.removeAllActions()
                                                                                                self.removeAllChildren()
                                                     })
//                let mainGameScreen = HardGameScore(size:self.size)
//                                                    self.view?.presentScene(mainGameScreen)
                                      }

    //
    //        if(self.cat.frame.intersects(self.bomb.frame)){
    //            catDeadAnimation()
    //            bombBlastAnimation()
    //            stopBackground = true
            //}
        }
        func powerUp(stone: SKSpriteNode) {
            var powerNode = SKSpriteNode(imageNamed: "power")

            powerNode.anchorPoint = CGPoint(x: 0.5,y: 0.5)
                  powerNode.position.x = stone.position.x
                  powerNode.position.y = stone.position.y
                  powerNode.size = CGSize(width: 67, height: 60)
                  powerNode.texture = SKTexture(imageNamed: "power.png")
                 
                  powerNode.zPosition = 998
            powerNode.name = "power"

            
              
                      powerNode.physicsBody = SKPhysicsBody(rectangleOf: powerNode.size)
                      powerNode.physicsBody?.affectedByGravity = false
                      powerNode.physicsBody?.categoryBitMask = 8
                      powerNode.physicsBody?.collisionBitMask = 0
                      powerNode.physicsBody?.contactTestBitMask = 1
                      powerNode.physicsBody?.allowsRotation = true
                 
                  
                  addChild(powerNode)
            self.powerArray.append(powerNode)
                  
                  let move1 = SKAction.move(to: CGPoint(x: stone.position.x , y: 200),
                                                    duration: 7)
                    
                          let stoneAnimation = SKAction.sequence(
                              [move1]
                          )
                        
            powerNode.run(stoneAnimation, completion: {
                powerNode.removeFromParent()
            })
            
        }
    func showLabel(){
               // showing the scores
               var currentScoreLabel : SKLabelNode?
                var highestScoreLabel : SKLabelNode?
                var currentScore = 0
               
               let background = SKSpriteNode(imageNamed: "bg")
               background.position = CGPoint(x: frame.size.width / 2, y:frame.size.height / 2)
                                 background.size = CGSize(width: frame.width, height: frame.height)
                                 background.anchorPoint = CGPoint.zero
                                 background.position = CGPoint(x: 0, y: 70)
                                 background.zPosition = 2
                                 self.addChild(background)
               //score labels
               currentScoreLabel = SKLabelNode(text: "Current Score: \(currentScore)")
                  currentScoreLabel!.position = CGPoint(x:300, y:590)
                  currentScoreLabel!.fontColor = UIColor.magenta
                  currentScoreLabel!.fontSize = 55
                  currentScoreLabel!.fontName = "Avenir"
               currentScoreLabel!.zPosition = 3

                  addChild(currentScoreLabel!)
                  
                  highestScoreLabel = SKLabelNode(text: "")
                  highestScoreLabel!.position = CGPoint(x:300, y:390)
                  highestScoreLabel!.fontColor = UIColor.magenta
                  highestScoreLabel!.fontSize = 55
                  highestScoreLabel!.fontName = "Avenir"
                highestScoreLabel!.zPosition = 3
                  addChild(highestScoreLabel!)
               // firestore
                 var db: Firestore!
                var ref = Firestore.firestore().collection("hardLevelScore")
               var a = 0
              
               print("Current score 111... \(currentScore)")
                               
                  currentScore = (self.defaults.integer(forKey: "HardLevelCurrentScore"))
               print("current score \(currentScore)")
                      
                      currentScoreLabel?.text = "Current Score: \(currentScore)"
                      
       //               self.highestScoreLabel?.text = "Highest Score: \(self.highestScore)"
                
               ref.addSnapshotListener{(snapshot, _) in
                                               guard let snapshot = snapshot else {return}
                                               for document in snapshot.documents {
                                                   print(" highest score: \(document.data()["HighestScore"] as! Int)")
                                                   a = document.data()["HighestScore"] as! Int
                                                   
                                                   if(currentScore > document.data()["HighestScore"] as! Int){
                                                                 
                                                       ref.document("1").setData(["HighestScore": currentScore])

                                                                }

                                                   highestScoreLabel?.text = "Highest Score: \(document.data()["HighestScore"] as! Int)"
                                               }
                                                                  
                                           }
                print("Highest score 111... \(a)")
           }
}

