//
//  MultiGameScene.swift
//  capstoneGame
//
//  Created by Satinder pal Singh on 2019-12-06.
//  Copyright © 2019 Prabhjinder. All rights reserved.
//

import Foundation
import SpriteKit
import GameplayKit
import FirebaseCore
import FirebaseFirestore
import AVFoundation
protocol MultiTransitionDelegate: SKSceneDelegate {
    func returnToMainMenu()
}

class MultiGameScene: SKScene, SKPhysicsContactDelegate {
    var enemy: SKSpriteNode!
    let defaults = UserDefaults.standard
    var player1 = ""
    var player2 = ""
    var backGround: SKSpriteNode!
    var finish: SKSpriteNode!
    var count = 0;
    var numBoxArray:[Int] = [Int]()
    var catdeadpause = false
    var audioplayer = AVAudioPlayer()
    var enemyPlayerBullets = false
    var enemyLives = 5
    var enemyLivesLabel : SKLabelNode!
    
    var playerBulletArray:[SKSpriteNode] = [SKSpriteNode]()
    var enemyBulletArray:[SKSpriteNode] = [SKSpriteNode]()

    deinit {
         print("\n THE SCENE \((type(of: self))) WAS REMOVED FROM MEMORY (DEINIT) \n")
    }
    
      func sound(name: String) {
       do{
           audioplayer = try AVAudioPlayer(contentsOf:URL.init(fileURLWithPath: Bundle.main.path(forResource: name, ofType: "wav")!) )
            audioplayer.prepareToPlay()
       }
        catch{
            print(error)
        }
        audioplayer.play()
    }

  
      
            private var label : SKLabelNode?
            private var spinnyNode : SKShapeNode?
//            let background1 = SKSpriteNode(imageNamed: "bg")
//            let background2 = SKSpriteNode(imageNamed: "bg")
            let cat = SKSpriteNode(imageNamed: "Run1")
            let dog = SKSpriteNode(imageNamed: "dogRun1")
            var bomb = SKSpriteNode(imageNamed: "bomb")

                var stopBackground = false
            var bombDirectionRight = true
            var counterTimer = Timer()
               var counter = 0
            var powerArray:[SKSpriteNode] = [SKSpriteNode]()
               var boxArray:[SKSpriteNode] = [SKSpriteNode]()
               var stoneArray:[SKSpriteNode] = [SKSpriteNode]()
             
            
            //adding the stones
            let stone1 = SKSpriteNode(imageNamed: "stone")
               let stone2 = SKSpriteNode(imageNamed: "stone")
               let stone3 = SKSpriteNode(imageNamed: "stone")
               let stone4 = SKSpriteNode(imageNamed: "stone")
               let stone5 = SKSpriteNode(imageNamed: "stone")
            
            var numStone1 = 10
            var numStone2 = 10
            var numStone3 = 10
            var numStone4 = 10
            var numStone5 = 10
            
            var stone1Label:SKLabelNode!
            var stone2Label:SKLabelNode!
            var stone3Label:SKLabelNode!
            var stone4Label:SKLabelNode!
            var stone5Label:SKLabelNode!
            
            var scoreLabel:SKLabelNode!
            var score = 0
            // lives label
             var livesLabel:SKLabelNode!
             var lives = 5
             
             // jumps label
             var jumpsLabel:SKLabelNode!
             var jumps = 50
            func definingphysics(){
                
                if(self.player1 == "cat"){
               // let catTexture = SKTexture(imageNamed: "Run1.png")
               // self.cat.physicsBody = SKPhysicsBody(texture: catTexture, size: catTexture.size())
                     self.cat.physicsBody = SKPhysicsBody(circleOfRadius: self.bomb.size.width/10)
                self.cat.physicsBody?.affectedByGravity = false
                self.cat.physicsBody?.categoryBitMask = 1
                self.cat.physicsBody?.collisionBitMask = 0
                self.cat.physicsBody?.contactTestBitMask = 6 + 64
                self.cat.physicsBody?.allowsRotation = false
                self.cat.name = "cat"
                }
                if(self.player1 == "dog"){
                              //let catTexture = SKTexture(imageNamed: "dogRun1.png")
                             // self.cat.physicsBody = SKPhysicsBody(texture: catTexture, size: catTexture.size())
                                   self.dog.physicsBody = SKPhysicsBody(circleOfRadius: self.bomb.size.width/10)
                              self.dog.physicsBody?.affectedByGravity = false
                              self.dog.physicsBody?.categoryBitMask = 1
                              self.dog.physicsBody?.collisionBitMask = 0
                              self.dog.physicsBody?.contactTestBitMask = 6 + 64
                              self.dog.physicsBody?.allowsRotation = false
                              self.dog.name = "dog"
                              }
                
                
            }
            
            
            func moveBackgroundLoop(){
                if (stopBackground == false && backGround.position.x > -1150) {

                   backGround.position = CGPoint(x: backGround.position.x-3, y: backGround.position.y)
                finish.position.x -= 3
                   //print(backGround.position.x)
                   backGround.zPosition = -999
                   print("hello 200")
                   print("hello :\(backGround.position.x.truncatingRemainder(dividingBy: 200))")
                   if(Int(backGround.position.x.truncatingRemainder(dividingBy: 200)) == 0)
                   {
                       print("hello 150")
                       
                       if(numBoxArray[count] == 1){
                         boxfirst(yValue: 160)
                       }
                       if(numBoxArray[count] == 2){
                         boxfirst(yValue: 160)
                          boxfirst(yValue: 235)
                       }
                       if(numBoxArray[count] == 3){
                         boxfirst(yValue: 160)
                         boxfirst(yValue: 235)
                         boxfirst(yValue: 310)
                       }
                       if(backGround.position.x <= -1000)
                       {
                             
                           stopBackground = true
                       }
                     count += 1
                   }
                   for (index, box) in self.boxArray.enumerated() {
                              box.position.x = box.position.x-3
                     
                             }

               }
                      
            
            }
            
            func spawnBomb(){
                if (bombDirectionRight == true)
                {
                    let throwBombAction = SKAction.applyImpulse(
                        CGVector(dx: 40, dy: 150),
                        duration: 1)
                    self.bomb.run(throwBombAction)

                }
                else{
                    let throwBombAction1 = SKAction.applyImpulse(
                        CGVector(dx: -80, dy: 150),
                        duration: 1)
                    self.bomb.run(throwBombAction1)
                }

                if(bomb.position.x >= 700)
                {
                    bombDirectionRight = false

                }
        //        else if(bomb.position.x <= 170)
        //        {
        //            bombDirectionRight = true
        //        }

            }
            
            override func didMove(to view: SKView) {
                
                var db: Firestore!
                var ref = Firestore.firestore().collection("lineCross")
                ref.document("1").setData(["name": "ok"])
                // enemy
                self.enemy = SKSpriteNode(imageNamed: "Idle (1)")
                self.enemy.position = CGPoint(x: 800, y:280)
                self.enemy.zPosition = 9999
                enemy.physicsBody = SKPhysicsBody(circleOfRadius: self.bomb.size.width/8)
                enemy.physicsBody?.affectedByGravity = false
                enemy.physicsBody?.categoryBitMask = 2
                enemy.physicsBody?.collisionBitMask = 0
                enemy.physicsBody?.contactTestBitMask = 1
                enemy.xScale = -1
                
                
               // print("backroung position is \(self.backGround.position.x)")
              //  print("cat1111 position is \(self.cat.position.x)")
                
                self.numBoxArray = [2,3,2,3,0,0,0,0,0,0]
                self.backGround = self.childNode(withName: "multibackground") as! SKSpriteNode
                self.finish = self.childNode(withName: "finish") as! SKSpriteNode
                let boxTexture = SKTexture(imageNamed: "box.png")
                     let box = SKSpriteNode(imageNamed: "box")
                     box.physicsBody = SKPhysicsBody(circleOfRadius: box.size.width/2.2)
                         //SKPhysicsBody(texture: boxTexture, size: boxTexture.size())
                     box.physicsBody?.affectedByGravity = false
                     box.physicsBody?.categoryBitMask = 2
                     box.physicsBody?.collisionBitMask = 0
                     box.physicsBody?.contactTestBitMask = 1
                     box.physicsBody?.allowsRotation = true
                     box.name = "box"
                     
                     box.position = CGPoint(x: 700, y:160+80)
                      
                    // self.boxArray[self.boxArray.count-1].position.x
                      box.size = CGSize(width: 160, height: 160)
                    //  box.anchorPoint = CGPoint.zero
                     box.zPosition = -13
                      self.addChild(box)
                     self.boxArray.append(box)
                    
                     let boxTexture1 = SKTexture(imageNamed: "box.png")
                     let box1 = SKSpriteNode(imageNamed: "box")
                     box1.physicsBody = SKPhysicsBody(circleOfRadius: box.size.width/2.2)
                         //SKPhysicsBody(texture: boxTexture, size: boxTexture.size())
                     box1.physicsBody?.affectedByGravity = false
                     box1.physicsBody?.categoryBitMask = 2
                     box1.physicsBody?.collisionBitMask = 0
                     box1.physicsBody?.contactTestBitMask = 1
                     box1.physicsBody?.allowsRotation = true
                     box1.name = "box"
                     
                     box1.position = CGPoint(x: 1300, y:160+80)
                      
                    // self.boxArray[self.boxArray.count-1].position.x
                      box1.size = CGSize(width: 160, height: 160)
                    //  box.anchorPoint = CGPoint.zero
                     box1.zPosition = -13
                      self.addChild(box1)
                     self.boxArray.append(box1)
                print("player name \(self.defaults.string(forKey: "Player")!)")
                // get value from user defaults
                if(self.defaults.string(forKey: "Player")! == "cat"){
                //self.player = SKSpriteNode(imageNamed: "Run1")
                    self.player1 = "cat"
                    self.player2 = "dog"
                    print("player selected cat")
                    
                    
                }
                if(self.defaults.string(forKey: "Player")! == "dog"){
               // self.player = SKSpriteNode(imageNamed: "dogRun1")
                    print("player selected dog")
                    self.player1 = "dog"
                self.player2 = "cat"
                }
                
                
                print("Player :\(self.player1)")
                
                self.physicsWorld.contactDelegate = self
                
                self.enemyLivesLabel = SKLabelNode(text: "Enemy Lives: \(self.enemyLives)")
                          self.enemyLivesLabel.position = CGPoint(x:960, y:590)
                          self.enemyLivesLabel.fontColor = UIColor.magenta
                          self.enemyLivesLabel.fontSize = 45
                          self.enemyLivesLabel.fontName = "Avenir"
                          addChild(self.enemyLivesLabel)
                
                
                // lives label
                self.livesLabel = SKLabelNode(text: "Lives: \(self.lives)")
                          self.livesLabel.position = CGPoint(x:120, y:590)
                          self.livesLabel.fontColor = UIColor.magenta
                          self.livesLabel.fontSize = 45
                          self.livesLabel.fontName = "Avenir"
                          addChild(self.livesLabel)
                
                // jumps label
                self.jumpsLabel = SKLabelNode(text: "Jumps: \(self.jumps)")
                          self.jumpsLabel.position = CGPoint(x:540, y:590)
                          self.jumpsLabel.fontColor = UIColor.magenta
                          self.jumpsLabel.fontSize = 45
                          self.jumpsLabel.fontName = "Avenir"
                          addChild(self.jumpsLabel)
                
                
                // Get label node from scene and store it for use later
                definingphysics()
                cat.position = CGPoint(x:self.size.width*0.25, y:240)
                addChild(cat)
                catAnimation()
               
               dog.position = CGPoint(x:self.size.width*0.25, y:290)
                     addChild(dog)
                     dogAnimation()
                startCounter()
                makeStone()
                makeBomb()
                
            }

            
            func didBegin(_ contact: SKPhysicsContact)
            {
                
                
                //print("Something collided!")
                let nodeA = contact.bodyA.node
                let nodeB = contact.bodyB.node
                if (nodeA == nil || nodeB == nil)
                {
                    return
                }
                if (nodeA!.name == "\(self.player1)" && nodeB!.name == "bomb" || nodeA!.name == "bomb" && nodeB!.name == "\(self.player1)")
                {
                    lives -= 1
                    print("lives remaining: \(lives)")
                    print("A: \(nodeA!.name)  b: \(nodeB!.name)")
                    if(self.player1 == "cat"){
                    catDeadAnimation()
                    }
                    if(self.player1 == "dog"){
                        dogDeadAnimation()
                    }
                    bombBlastAnimation()
                    stopBackground = true
                }
                else if(nodeA!.name == "\(self.player1)" && nodeB!.name == "box" || nodeA!.name == "box" && nodeB!.name == "\(self.player1)")
                {
                    lives -= 1
                    print("A: \(nodeA!.name)  b: \(nodeB!.name)")
                    
                    stopBackground = true
                    //box.removeFromParent()
                    //self.boxArray.remove(at:index)
                    
                   if(self.player1 == "cat"){
                                                         catDeadAnimation()
                                          cat.position = CGPoint(x:self.size.width*0.22, y:270)
                                                         }
                                                         if(self.player1 == "dog"){
                                                             dogDeadAnimation()
                        dog.position = CGPoint(x:self.size.width*0.22, y:270)
                                                         }
                    if(nodeA!.name == "box" )
                    {
                        nodeA?.removeFromParent()
                        for (index, box) in self.boxArray.enumerated()
                        {
                            if(box.position.x == nodeA?.position.x)
                            {
                                box.removeFromParent()
                            }
                        }
                    }
                    else if(nodeB!.name == "box")
                    {
                        nodeB?.removeFromParent()
                        for (index, box) in self.boxArray.enumerated()
                        {
                            if(box.position.x == nodeB?.position.x)
                            {
                                box.removeFromParent()
                            }
                        }
                    }
                    
                }
                else if(nodeA!.name == "\(self.player1)" && nodeB!.name == "power" || nodeA!.name == "power" && nodeB!.name == "\(self.player1)")
                {
                    print("A: \(nodeA!.name)  b: \(nodeB!.name)")
//                    self.jumps += 5
//                    powerNode.removeFromParent()
                }
                else if(nodeA!.name == "finish" && nodeB!.name == "cat" || nodeA!.name == "cat" && nodeB!.name == "finish")
                {
                    self.enemyPlayerBullets = true
                    addChild(enemy)
                    
                    // firestore
                     var db: Firestore!
                    var ref = Firestore.firestore().collection("lineCross")
                    ref.document("1").setData(["name": "cat"])


                    print("Cat Won \(backGround.position.x)")
                  
                     for (index, box) in self.boxArray.enumerated()
                                                {
                                                    box.removeFromParent()
                                                           print("stone array size1: \(self.stoneArray.count)")
                                                           bombBlastAnimation()
                                                           print("collision between stone and bomb")
                                                       
                                               }
                    boxArray.removeAll()
                    finish.removeFromParent()
                    dog.removeFromParent()
                    
                    sound(name: "win")
                }
                else if(nodeA!.name == "finish" && nodeB!.name == "dog" || nodeA!.name == "dog" && nodeB!.name == "finish")
                {
                    // firestore
                     var db: Firestore!
                    var ref = Firestore.firestore().collection("lineCross")
                    ref.document("1").setData(["name": "dog"])
                    self.enemyPlayerBullets = true
                    addChild(enemy)

                    print("Dog Won")
                     for (index, box) in self.boxArray.enumerated()
                                                {
                                                    box.removeFromParent()
                                                           print("stone array size1: \(self.stoneArray.count)")
                                                           bombBlastAnimation()
                                                           print("collision between stone and bomb")
                                                       
                                               }
                    boxArray.removeAll()
                    finish.removeFromParent()
                    cat.removeFromParent()
                    
                    sound(name: "win")
                    
                }
                
       

            }
            func makeBomb(){
                self.bomb = SKSpriteNode(imageNamed: "bomb")
                // self.bomb.anchorPoint = CGPoint.zero
                self.bombDirectionRight = true
                                self.bomb.position.x = -50
                                self.bomb.position.y = 590
                                self.bomb.size = CGSize(width: 100, height: 95)
                                self.bomb.texture = SKTexture(imageNamed: "bomb.png")
                bomb.name = "bomb"
                                self.bomb.zPosition = 999
                      
                      self.bomb.physicsBody = SKPhysicsBody(circleOfRadius: self.bomb.size.width/3)
                           self.bomb.physicsBody?.affectedByGravity = true
                           self.bomb.physicsBody?.isDynamic = true
                           self.bomb.physicsBody?.categoryBitMask = 4
                           self.bomb.physicsBody?.restitution = 1
                           self.bomb.physicsBody?.collisionBitMask = 16
                           self.bomb.physicsBody?.contactTestBitMask = 9
                           self.bomb.physicsBody?.allowsRotation = true
                addChild(self.bomb)
                spawnBomb()
            }
            func makeStone(){
                stone1.position = CGPoint(x: 143.5, y:110)
                stone1.size = CGSize(width: 67, height: 60)
                stone1.anchorPoint = CGPoint(x: 0.5,y: 0.5)
                stone1.zPosition = -13
                self.addChild(stone1)
                
                
                stone2.position = CGPoint(x: 403.5, y:110)
                stone2.size = CGSize(width: 67, height: 60)
                stone2.anchorPoint = CGPoint(x: 0.5,y: 0.5)
                stone2.zPosition = -13
                self.addChild(stone2)
                
                stone3.position = CGPoint(x: 670.5, y:110)
                stone3.size = CGSize(width: 67, height: 60)
                stone3.anchorPoint = CGPoint(x: 0.5,y: 0.5)
                stone3.zPosition = -13
                self.addChild(stone3)
                
                stone4.position = CGPoint(x: 937.5, y:110)
                stone4.size = CGSize(width: 67, height: 60)
                stone4.anchorPoint = CGPoint(x: 0.5,y: 0.5)
                stone4.zPosition = -13
                self.addChild(stone4)
                
                stone5.position = CGPoint(x: 1204.5, y:110)
                stone5.size = CGSize(width: 67, height: 60)
                stone5.anchorPoint = CGPoint(x: 0.5,y: 0.5)
                stone5.zPosition = -13
                self.addChild(stone5)
                
                
                // adding number label on stones
                self.stone1Label = SKLabelNode(text: "\(self.numStone1)")
                       self.stone1Label.position = CGPoint(x:120, y:90)
                       self.stone1Label.fontColor = UIColor.magenta
                       self.stone1Label.fontSize = 55
                       self.stone1Label.fontName = "Avenir"
                       addChild(self.stone1Label)
                
                self.stone2Label = SKLabelNode(text: "\(self.numStone2)")
                self.stone2Label.position = CGPoint(x:380, y:90)
                self.stone2Label.fontColor = UIColor.magenta
                self.stone2Label.fontSize = 55
                self.stone2Label.fontName = "Avenir"
                addChild(self.stone2Label)
                
                self.stone3Label = SKLabelNode(text: "\(self.numStone3)")
                self.stone3Label.position = CGPoint(x:647, y:90)
                self.stone3Label.fontColor = UIColor.magenta
                self.stone3Label.fontSize = 55
                self.stone3Label.fontName = "Avenir"
                addChild(self.stone3Label)
                
                self.stone4Label = SKLabelNode(text: "\(self.numStone4)")
                self.stone4Label.position = CGPoint(x:914, y:90)
                self.stone4Label.fontColor = UIColor.magenta
                self.stone4Label.fontSize = 55
                self.stone4Label.fontName = "Avenir"
                addChild(self.stone4Label)
                
                self.stone5Label = SKLabelNode(text: "\(self.numStone5)")
                self.stone5Label.position = CGPoint(x:1181, y:90)
                self.stone5Label.fontColor = UIColor.magenta
                self.stone5Label.fontSize = 55
                self.stone5Label.fontName = "Avenir"
                addChild(self.stone5Label)
                
            }
            
            var boxCount = 0
            func boxfirst(yValue: CGFloat)
            {
                let boxTexture = SKTexture(imageNamed: "box.png")
                let box = SKSpriteNode(imageNamed: "box")
                box.physicsBody = SKPhysicsBody(circleOfRadius: box.size.width/2.2)
                    //SKPhysicsBody(texture: boxTexture, size: boxTexture.size())
                box.physicsBody?.affectedByGravity = false
                box.physicsBody?.categoryBitMask = 2
                box.physicsBody?.collisionBitMask = 0
                box.physicsBody?.contactTestBitMask = 1
                box.physicsBody?.allowsRotation = true
                box.name = "box"
                
                box.position = CGPoint(x: size.width+50, y:yValue+70)
                 
               // self.boxArray[self.boxArray.count-1].position.x
                 box.size = CGSize(width: 160, height: 160)
               //  box.anchorPoint = CGPoint.zero
                box.zPosition = -13
                 self.addChild(box)
                self.boxArray.append(box)
                         print(boxArray.count)
                
            }
            
            func startCounter(){
                       counterTimer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(incrementCounter), userInfo: nil, repeats: true)
                   }
                   @objc func incrementCounter(){
                    self.counter = self.counter + 1
                    
                  
              
                    
                    if(self.bomb.position.x <= 0){
                        bomb.removeFromParent()
                        print("bomb disappear position less than 0")
                        makeBomb()
                    }
                    
                    
                    
                    
                    // firestore
                     var db: Firestore!
                   var ref = Firestore.firestore().collection("lineCross")
                    ref.addSnapshotListener{(snapshot, _) in
                                            guard let snapshot = snapshot else {return}
                                            for document in snapshot.documents {
                                                print(document.data()["name"])
                                                if(document.data()["name"] as! String == "cat"){
                                                    if(self.player1 == "dog"){
                                                        self.lives = 0
                                                    }
                                                    
                                                    
                                                }
                                                else{}
                                                if(document.data()["name"] as! String == "dog"){
                                                    if(self.player1 == "cat"){
                                                        self.lives = 0
                                                    }
                                                    
                                                    
                                                }
                                                else{}
                                           
                                            }
                                        }
                    
                    ref = Firestore.firestore().collection("cat")
                    
                    if(self.player2 == "dog"){
                  print("cat position is \(self.cat.position.x)")
                                           print("dog position is \(self.dog.position.x)")
                                            print("background position is \(self.backGround.position.x)")

                       // ref.document("1").setData(["x": [self.cat.position.x, self.backGround.position.x],"y":self.cat.position.y])
  ref.document("1").setData(["x": self.cat.position.x + self.backGround.position.x,"y":self.cat.position.y])
                        ref = Firestore.firestore().collection("dog")
                        ref.addSnapshotListener{(snapshot, _) in
                            guard let snapshot = snapshot else {return}
                            for document in snapshot.documents {
                                print(document.data()["x"])
                                self.dog.position.x = -(document.data()["x"] as! CGFloat) + self.backGround.position.x + 750
                                self.dog.position.y = document.data()["y"] as! CGFloat
                                
                              
                            }
                        }
                    }
                    if(self.player2 == "cat"){
                        print("cat position is \(self.cat.position.x)")
                        print("dog position is \(self.dog.position.x)")
                         print("background position is \(self.backGround.position.x)")
                                     ref = Firestore.firestore().collection("dog")
 
                   // ref.document("2").setData(["x": [self.cat.position.x, self.backGround.position.x],"y":self.cat.position.y])
                    ref.document("2").setData(["x": self.dog.position.x + self.backGround.position.x,"y":self.dog.position.y])

                        ref = Firestore.firestore().collection("cat")
                                           ref.addSnapshotListener{(snapshot, _) in
                                               guard let snapshot = snapshot else {return}
                                               for document in snapshot.documents {
                                                   print(document.data()["x"])
                                                self.cat.position.x = -(document.data()["x"] as! CGFloat) + self.backGround.position.x + 750
                                                   self.cat.position.y = document.data()["y"] as! CGFloat
                                                
                                             
                                               }
                                           }
                                       }
                    
                    self.score = self.score + 1
                    
                    spawnBomb()
                    if(self.counter % 3 == 0 && self.stopBackground == false){
                    let randomInt = Int.random(in: 1...3)
                        
                    print("random number \(randomInt)")
                      
                    }
                    
                }
            
            
            func stopTimer() {
                counterTimer.invalidate()
                //counterTimer = nil
            }
            
            
            
            func catAnimation(){
                if(self.stopBackground == false)
                {
                    let image1 = SKTexture(imageNamed: "Run1")
                    let image2 = SKTexture(imageNamed: "Run2")
                    let image3 = SKTexture(imageNamed: "Run3")
                    let image4 = SKTexture(imageNamed: "Run4")
                    let image5 = SKTexture(imageNamed: "Run5")
                    let image6 = SKTexture(imageNamed: "Run6")
                    let image7 = SKTexture(imageNamed: "Run7")
                    let image8 = SKTexture(imageNamed: "Run8")
                    let punchTextures = [image1,image2,image3,image4,image5,image6,image7,image8]
                    let punchAnimation = SKAction.animate(
                                   with: punchTextures,
                                   timePerFrame: 0.1)
                    self.cat.run(punchAnimation)
                    let catAnimationForeever = SKAction.repeatForever(punchAnimation)
                    self.cat.run(catAnimationForeever)
                    cat.zPosition = 1
                }
            }
           
            func catJumpAnimation(){
                sound(name: "jump")
                let image1 = SKTexture(imageNamed: "Jump1")
                let image2 = SKTexture(imageNamed: "Jump4")
             //   let image3 = SKTexture(imageNamed: "Jump3")
                let image4 = SKTexture(imageNamed: "Jump2")
                let punchTextures = [image1,image2,image4]
                let punchAnimation = SKAction.animate(
                               with: punchTextures,
                               timePerFrame: 0.6)
                self.cat.run(punchAnimation)
                cat.zPosition = 1
                
            }
              func catDeadAnimation(){
                sound(name: "death")
                    catdeadpause = true
                    let image1 = SKTexture(imageNamed: "catdead1")
                    let image2 = SKTexture(imageNamed: "catdead2")
                    let image3 = SKTexture(imageNamed: "catdead3")
                    let image4 = SKTexture(imageNamed: "catdead4")
                    let punchTextures = [image1,image2,image3,image4]
                    let punchAnimation = SKAction.animate(
                                   with: punchTextures,
                                   timePerFrame: 0.2)
                    self.cat.run(punchAnimation, completion:{ self.catdeadpause = false
                        
                    })
                    cat.zPosition = 1
                
                    
                }
    func dogAnimation(){
          let image1 = SKTexture(imageNamed: "dogRun1")
          let image2 = SKTexture(imageNamed: "dogRun2")
          let image3 = SKTexture(imageNamed: "dogRun3")
          let image4 = SKTexture(imageNamed: "dogRun4")
          let image5 = SKTexture(imageNamed: "dogRun5")
          let image6 = SKTexture(imageNamed: "dogRun6")
          let image7 = SKTexture(imageNamed: "dogRun7")
          let image8 = SKTexture(imageNamed: "dogRun8")
          let punchTextures = [image1,image2,image3,image4,image5,image6,image7,image8]
          let punchAnimation = SKAction.animate(
                         with: punchTextures,
                         timePerFrame: 0.1)
          self.dog.run(punchAnimation)
          let dogAnimationForeever = SKAction.repeatForever(punchAnimation)
          self.dog.run(dogAnimationForeever)
         
     }

         
          func dogJumpAnimation(){
              sound(name: "jump")
              let image1 = SKTexture(imageNamed: "dogJump1")
              let image2 = SKTexture(imageNamed: "dogJump4")
           //   let image3 = SKTexture(imageNamed: "Jump3")
              let image4 = SKTexture(imageNamed: "dogJump2")
              let punchTextures = [image1,image2,image4]
              let punchAnimation = SKAction.animate(
                             with: punchTextures,
                             timePerFrame: 0.6)
              self.dog.run(punchAnimation)
              dog.zPosition = 1
              
          }
            func dogDeadAnimation(){
              sound(name: "death")
                  catdeadpause = true
                  let image1 = SKTexture(imageNamed: "dogdead1")
                  let image2 = SKTexture(imageNamed: "dogdead2")
                  let image3 = SKTexture(imageNamed: "dogdead3")
                  let image4 = SKTexture(imageNamed: "dogdead4")
                  let punchTextures = [image1,image2,image3,image4]
                  let punchAnimation = SKAction.animate(
                                 with: punchTextures,
                                 timePerFrame: 0.2)
                  self.dog.run(punchAnimation, completion:{ self.catdeadpause = false})
                  dog.zPosition = 1
                  
              }
                func bombBlastAnimation(){
                    sound(name: "bombExplosion")
                      let image1 = SKTexture(imageNamed: "pow")
                                 let punchTextures = [image1]
                                 let punchAnimation = SKAction.animate(
                                                with: punchTextures,
                                                timePerFrame: 0.2)
                    self.bomb.run(punchAnimation, completion: {
                        self.bomb.removeFromParent()
                        self.makeBomb()
                    })
                      
                  }
            func touchDown(atPoint pos : CGPoint) {
            }
            
            func touchMoved(toPoint pos : CGPoint) {
            }
            
            func touchUp(atPoint pos : CGPoint) {
                if let n = self.spinnyNode?.copy() as! SKShapeNode? {
                    n.position = pos
                    n.strokeColor = SKColor.red
                    self.addChild(n)
                }
            }
            var counterJump = 0;
            func jumpingcatdog()  {
                if(self.player1 == "cat"){
                print("jump")
                self.jumps -= 1
                print(jumps)
                
                catJumpAnimation()
                let jumpUpAction = SKAction.moveBy(x: 0, y:150, duration:0.9)
                let jumpDownAction = SKAction.moveBy(x: 0, y:-150, duration:0.9 )
                let jumpSequence = SKAction.sequence([jumpUpAction, jumpDownAction])
                cat.run(jumpSequence)
                }
                if(self.player1 == "dog"){
                               print("jump")
                               self.jumps -= 1
                               print(jumps)
                               
                               dogJumpAnimation()
                               let jumpUpAction = SKAction.moveBy(x: 0, y:150, duration:0.9)
                               let jumpDownAction = SKAction.moveBy(x: 0, y:-150, duration:0.9 )
                               let jumpSequence = SKAction.sequence([jumpUpAction, jumpDownAction])
                               dog.run(jumpSequence)
                               }
            }
            override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
               // let location = (touches.first as! UITouch).location(in: self.view)
                // stone tapped
                
                 
                      guard let mousePosition = touches.first?.location(in: self) else {
                            return
                        }
                if(catdeadpause == false)
                   
                        {
                            if(Int(self.stone1Label.text!)! > 0){
                            if self.stone1.contains(mousePosition){
                            self.numStone1 = self.numStone1 - 1
                            self.stone1Label.text = "\(self.numStone1)"
                            spawnStone(stone: self.stone1)
                                }
                        }
                            if(Int(self.stone2Label.text!)! > 0){
                        
                         if self.stone2.contains(mousePosition){
                            self.numStone2 = self.numStone2 - 1
                            self.stone2Label.text = "\(self.numStone2)"
                            spawnStone(stone: self.stone2)
                        }
                            }
                        if(Int(self.stone3Label.text!)! > 0){
                         if self.stone3.contains(mousePosition){
                            self.numStone3 = self.numStone3 - 1
                            self.stone3Label.text = "\(self.numStone3)"
                            spawnStone(stone: self.stone3)
                        }
                            }
                            
                            if(Int(self.stone4Label.text!)! > 0){
                         if self.stone4.contains(mousePosition){
                            self.numStone4 = self.numStone4 - 1
                            self.stone4Label.text = "\(self.numStone4)"
                            spawnStone(stone: self.stone4)
                        }
                            }
                            if(Int(self.stone5Label.text!)! > 0){
                         if self.stone5.contains(mousePosition){
                            self.numStone5 = self.numStone5 - 1
                            self.stone5Label.text = "\(self.numStone5)"
                            spawnStone(stone: self.stone5)
                        }
                            }
                     if (mousePosition.y > (self.size.height)/2){
                        jumpingcatdog()
                            
                    }
                    else{
                            if(self.player1 == "cat"){
                    if mousePosition.x < (self.size.width)/2 {
                        print("left")
                        stopBackground = true
                        if(self.cat.position.x >= 100)
                        {
                            self.cat.position.x -= 15;
                            print("\(self.cat.position.x)");

                        }
                       // MoveLeft()
                    } else {
                        // right code

                           print("right")
                        if(self.cat.position.x <= (self.size.width)*0.25)
                                     {
                                         stopBackground = true
                                     }
                        else
                        {
                             stopBackground = false
                        }
                        if(self.cat.position.x <= (self.size.width)/1.5)
                        {
                            self.cat.position.x += 15;
                            print("\(self.cat.position.x)");
                        }

                    }
                }
                            if(self.player1 == "dog"){
                                if mousePosition.x < (self.size.width)/2 {
                                    print("left")
                                    stopBackground = true
                                    if(self.dog.position.x >= 100)
                                    {
                                        self.dog.position.x -= 15;
                                        print("\(self.dog.position.x)");

                                    }
                                   // MoveLeft()
                                } else {
                                    // right code
                                    
                                       print("right")
                                    if(self.dog.position.x <= (self.size.width)*0.25)
                                                 {
                                                     stopBackground = true
                                                 }
                                    else
                                    {
                                         stopBackground = false
                                    }
                                    if(self.dog.position.x <= (self.size.width)/1.5)
                                    {
                                        self.dog.position.x += 15;
                                        print("\(self.dog.position.x)");
                                    }

                                }
                            }
                            }
                            
                }
                
              
            }
            func spawnStone(stone : SKSpriteNode) {
                   
                //adding the stone
                   let stoneX = SKSpriteNode(imageNamed: "stone")
                stoneX.anchorPoint = CGPoint(x: 0.5,y: 0.5)
                stoneX.position.x = stone.position.x
                stoneX.position.y = stone.position.y
                stoneX.size = CGSize(width: 67, height: 60)
                stoneX.texture = SKTexture(imageNamed: "stone.png")
               
                stoneX.zPosition = 999

          
            
                    stoneX.physicsBody = SKPhysicsBody(rectangleOf: stoneX.size)
                    stoneX.physicsBody?.affectedByGravity = false
                    stoneX.physicsBody?.categoryBitMask = 8
                    stoneX.physicsBody?.collisionBitMask = 0
                    stoneX.physicsBody?.contactTestBitMask = 4
                    stoneX.physicsBody?.allowsRotation = true
               
                
                addChild(stoneX)
                self.stoneArray.append(stoneX)
                
                   
                let move1 = SKAction.move(to: CGPoint(x: stone.position.x , y: size.height),
                                                  duration: 2)
                  
                        let stoneAnimation = SKAction.sequence(
                            [move1]
                        )
                      
                        stoneX.run(stoneAnimation)
                   
                   
               }
            
            override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
                for t in touches { self.touchMoved(toPoint: t.location(in: self)) }
            }
            
            override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
                for t in touches { self.touchUp(atPoint: t.location(in: self)) }
            }
            
            override func touchesCancelled(_ touches: Set<UITouch>, with event: UIEvent?) {
                for t in touches { self.touchUp(atPoint: t.location(in: self)) }
            }
            
            var count1 = 0
            override func update(_ currentTime: TimeInterval) {
                if(self.lives == 0){
                    catDeadAnimation()
                  //  self.cat.removeFromParent()
                    
                }
                if(self.enemyLives == 0){
                     enemyDeadAnimation()
                    //  self.cat.removeFromParent()
                                  
                              }
                
                
                if(self.enemyLives > 0){
                self.enemyLivesLabel.text = "Enemy Lives: \(self.enemyLives)"
                }
                else{
                                        self.enemyLivesLabel.text = "Enemy Lives: 0"
                }
                if(self.lives > 0){
                                  self.livesLabel.text = "Lives: \(self.lives)"
                                  }
                                  else{
                                                          self.livesLabel.text = "Lives: 0"
                                  }
                self.jumpsLabel.text = "Jump: \(self.jumps)"
              
                if(self.count1 % 100 == 0){
                     
                    print("bullets1")
                if(self.enemyPlayerBullets == true){
                    // spawn the bullet
                    print("bullets")
                    if(self.lives > 0){
                                      spawnBullet()
                    }
                    if(self.enemyLives > 0){
                                      spawnBulletEnemy()
                    }
                }
                   
                }
                 if(self.count1 % 200 == 0){
                    if(self.enemyLives > 0){
 enemyJumpAnimation()
                        
                    }
               
                }
                 self.count1 = self.count1 + 1
        //        if(self.stone1Label.text == "0"){
        //        self.stone1.removeFromParent()
        //        }
                // Called before each frame is rendered
                if(stopBackground == false)
                {
                    moveBackgroundLoop()
                }
       
                             for (index, stone) in self.stoneArray.enumerated()
                             {
                                if (self.bomb.frame.intersects(stone.frame) == true)
                                     {
                                        
                                        print("stone array size: \(self.stoneArray.count)")
                                        
                                        powerUp(stone: stone)
                                                               stone.removeFromParent()
                                        if(self.stoneArray.count > index){
                                                self.stoneArray.remove(at:index)
                                        }
                                        else{
                                            print("jugaad")
                                        }
                           
                                        print("stone array size1: \(self.stoneArray.count)")
                                        bombBlastAnimation()
                                        print("collision between stone and bomb")
                                    }
                            }
          bulletsIntersection()
                for (index, playerBullet) in self.playerBulletArray.enumerated()
                  {
                     if (self.enemy.frame.intersects(playerBullet.frame) == true)
                          {
                            if(self.enemyLives == 0){
                            print("okokokok")
                                          self.enemyDeadAnimation()
                                      }
                             
                            
                                                    playerBullet.removeFromParent()
                             if(self.playerBulletArray.count > index){
                                     self.playerBulletArray.remove(at:index)
                             }
                             else{
                                 print("jugaad")
                             }
                
                             print("stone array size1: \(self.stoneArray.count)")
                             
                            
                           self.enemyLives = self.enemyLives - 1
                                print("enemy lives \(self.enemyLives)")
                            
                             print("collision between stone and bomb")
                         }
                 }
                
           
              
                if(enemyLives == 0){
                                    self.run(SKAction.wait(forDuration: 1),completion:{[unowned self] in
                                                   guard let delegate = self.delegate else { return }
                                                   self.view?.presentScene(nil)
                                                   (delegate as! MultiTransitionDelegate).returnToMainMenu()
                                        self.sound(name: "win")
                                              self.stopTimer()
                                        self.removeAllActions()
                                        self.removeAllChildren()
                                               })
                    showResultLabel()
                                }
                        if(lives == 0){
                                                self.run(SKAction.wait(forDuration: 1),completion:{[unowned self] in
                                                               guard let delegate = self.delegate else { return }
                                                               self.view?.presentScene(nil)
                                                               (delegate as! MultiTransitionDelegate).returnToMainMenu()
                                                          self.stopTimer()
                                                    self.removeAllActions()
                                                                                           self.removeAllChildren()
                                                           })
                            showResultLabel()
                                            }
                if(jumps == 0){
                                              self.run(SKAction.wait(forDuration: 1),completion:{[unowned self] in
                                                             guard let delegate = self.delegate else { return }
                                                             self.view?.presentScene(nil)
                                                             (delegate as! MultiTransitionDelegate).returnToMainMenu()
                                                        self.stopTimer()
                                                self.removeAllActions()
                                                                                       self.removeAllChildren()
                                                         })
                    showResultLabel()
                                          }

        //
        //        if(self.cat.frame.intersects(self.bomb.frame)){
        //            catDeadAnimation()
        //            bombBlastAnimation()
        //            stopBackground = true
                //}
            }
    
    func bulletsIntersection(){
        if(self.player1 == "cat"){
                  for (index, enemyBullet) in self.enemyBulletArray.enumerated()
                                                  {
                                                     if (self.cat.frame.intersects(enemyBullet.frame) == true)
                                                          {
                                                             
                                                             if(self.lives == 0){
                                                                                print("okokokok1111")
                                                                                              self.catDeadAnimation()
            //                                                    self.cat.removeFromParent()
            //                                                    self.livesLabel.text = "Losser!"
                                                                                          }
                                                            
                                                                                    enemyBullet.removeFromParent()
                                                             if(self.enemyBulletArray.count > index){
                                                                     self.enemyBulletArray.remove(at:index)
                                                             }
                                                             else{
                                                                 print("jugaad")
                                                             }
                                                
                                                             print("stone array size1: \(self.stoneArray.count)")
                                                           
                                                                                      self.lives = self.lives - 1
                                                                                          // print("enemy lives \(self.enemyLives)")
                                                                                       
                                                             print("collision between stone and bomb")
                                                         }
                                                 }
            
            //power
            for (index, power) in self.powerArray.enumerated()
                         {
                            if (self.cat.frame.intersects(power.frame) == true)
                                 {
                                    
                                    print("stone array size: \(self.stoneArray.count)")
                                    
                                   
                                    if(self.powerArray.count > index){
                                       power.removeFromParent()

                                            self.powerArray.remove(at:index)
                                       self.jumps += 5
                                    }
                                    else{
                                       //power.removeFromParent()
                                        print("jugaad")
                                    }
                       
                                    print("stone array size1: \(self.powerArray.count)")
                                  //  bombBlastAnimation()
                                    print("collision between stone and bomb")
                                }
                        }
        }
        if(self.player1 == "dog"){
                  for (index, enemyBullet) in self.enemyBulletArray.enumerated()
                                                  {
                                                     if (self.dog.frame.intersects(enemyBullet.frame) == true)
                                                          {
                                                             
                                                             if(self.lives == 0){
                                                                                print("okokokok1111")
                                                                                              self.dogDeadAnimation()
            //                                                    self.cat.removeFromParent()
            //                                                    self.livesLabel.text = "Losser!"
                                                                                          }
                                                            
                                                                                    enemyBullet.removeFromParent()
                                                             if(self.enemyBulletArray.count > index){
                                                                     self.enemyBulletArray.remove(at:index)
                                                             }
                                                             else{
                                                                 print("jugaad")
                                                             }
                                                
                                                             print("stone array size1: \(self.stoneArray.count)")
                                                           
                                                                                      self.lives = self.lives - 1
                                                                                          // print("enemy lives \(self.enemyLives)")
                                                                                       
                                                             print("collision between stone and bomb")
                                                         }
                                                 }
            // power
            for (index, power) in self.powerArray.enumerated()
                         {
                            if (self.dog.frame.intersects(power.frame) == true)
                                 {
                                    
                                    print("stone array size: \(self.stoneArray.count)")
                                    
                                   
                                    if(self.powerArray.count > index){
                                       power.removeFromParent()

                                            self.powerArray.remove(at:index)
                                       self.jumps += 5
                                    }
                                    else{
                                       //power.removeFromParent()
                                        print("jugaad")
                                    }
                       
                                    print("stone array size1: \(self.powerArray.count)")
                                  //  bombBlastAnimation()
                                    print("collision between stone and bomb")
                                }
                        }
        }
    }
            func powerUp(stone: SKSpriteNode) {
                var powerNode = SKSpriteNode(imageNamed: "power")

                powerNode.anchorPoint = CGPoint(x: 0.5,y: 0.5)
                      powerNode.position.x = stone.position.x
                      powerNode.position.y = stone.position.y
                      powerNode.size = CGSize(width: 67, height: 60)
                      powerNode.texture = SKTexture(imageNamed: "power.png")
                     
                      powerNode.zPosition = 998
                powerNode.name = "power"

                
                  
                          powerNode.physicsBody = SKPhysicsBody(rectangleOf: powerNode.size)
                          powerNode.physicsBody?.affectedByGravity = false
                          powerNode.physicsBody?.categoryBitMask = 8
                          powerNode.physicsBody?.collisionBitMask = 0
                          powerNode.physicsBody?.contactTestBitMask = 1
                          powerNode.physicsBody?.allowsRotation = true
                     
                      
                      addChild(powerNode)
                self.powerArray.append(powerNode)
                      
                      let move1 = SKAction.move(to: CGPoint(x: stone.position.x , y: 200),
                                                        duration: 7)
                        
                              let stoneAnimation = SKAction.sequence(
                                  [move1]
                              )
                            
                powerNode.run(stoneAnimation, completion: {
                    powerNode.removeFromParent()
                })
                
            }
    
      func spawnBullet() {
             
          //adding the bullet
        if(self.player1 == "cat"){
      let bullet = SKSpriteNode(imageNamed: "bullet")
                   bullet.anchorPoint = CGPoint(x: 0.5,y: 0.5)
                 bullet.position.x = self.cat.position.x
                 bullet.position.y = self.cat.position.y
                   bullet.size = CGSize(width: 67, height: 60)
                   bullet.texture = SKTexture(imageNamed: "bullet.png")
                 bullet.xScale = -1
                  
                   bullet.zPosition = 999

             
               
                       bullet.physicsBody = SKPhysicsBody(rectangleOf: bullet.size)
                       bullet.physicsBody?.affectedByGravity = false
                       bullet.physicsBody?.categoryBitMask = 8
                       bullet.physicsBody?.collisionBitMask = 0
                       bullet.physicsBody?.contactTestBitMask = 4
                  
                   
                   addChild(bullet)
                 self.playerBulletArray.append(bullet)
                   
                      
        let move1 = SKAction.move(to: CGPoint(x: self.size.width + 40, y: self.cat.position.y),
                                                     duration: 3)
                     
                           let stoneAnimation = SKAction.sequence(
                               [move1]
                           )
                         
                           bullet.run(stoneAnimation)
        }
        if(self.player1 == "dog"){
             let bullet = SKSpriteNode(imageNamed: "bullet")
                          bullet.anchorPoint = CGPoint(x: 0.5,y: 0.5)
                        bullet.position.x = self.dog.position.x
                        bullet.position.y = self.dog.position.y
                          bullet.size = CGSize(width: 67, height: 60)
                          bullet.texture = SKTexture(imageNamed: "bullet.png")
                        bullet.xScale = -1
                         
                          bullet.zPosition = 999

                    
                      
                              bullet.physicsBody = SKPhysicsBody(rectangleOf: bullet.size)
                              bullet.physicsBody?.affectedByGravity = false
                              bullet.physicsBody?.categoryBitMask = 8
                              bullet.physicsBody?.collisionBitMask = 0
                              bullet.physicsBody?.contactTestBitMask = 4
                         
                          
                          addChild(bullet)
                        self.playerBulletArray.append(bullet)
                          
                             
               let move1 = SKAction.move(to: CGPoint(x: self.size.width + 40, y: self.dog.position.y),
                                                            duration: 3)
                            
                                  let stoneAnimation = SKAction.sequence(
                                      [move1]
                                  )
                                
                                  bullet.run(stoneAnimation)
               }
         }
    // enemy.....................

//    func enemyShootAnimation(){
//        sound(name: "bombExplosion")
//        let image1 = SKTexture(imageNamed: "Shoot (4)")
//        let punchTextures = [image1]
//        let punchAnimation = SKAction.animate(
//                       with: punchTextures,
//                       timePerFrame: 0.6)
//        self.enemy.run(punchAnimation)
//        enemy.zPosition = 1
//
//    }
//    func enemyRunShootAnimation(){
//        sound(name: "jump")
//
//        let image1 = SKTexture(imageNamed: "RunShoot (2)")
//        let image2 = SKTexture(imageNamed: "RunShoot (3)")
//        let image3 = SKTexture(imageNamed: "RunShoot (5)")
//        let image4 = SKTexture(imageNamed: "RunShoot (6)")
//        let punchTextures = [image1,image2,image3,image4]
//        let punchAnimation = SKAction.animate(
//                       with: punchTextures,
//                       timePerFrame: 0.3)
//        self.enemy.run(punchAnimation)
//        enemy.zPosition = 1
//        let Action = SKAction.moveBy(x: -300, y:0, duration:1.2)
//                           let jumpSequence = SKAction.sequence([Action])
//                           enemy.run(jumpSequence)
//
//    }
    func enemyJumpAnimation(){
        
      
        
        sound(name: "jump")
        let image1 = SKTexture(imageNamed: "Jump (1)")
        let image2 = SKTexture(imageNamed: "Jump (3)")
        let image3 = SKTexture(imageNamed: "JumpShoot (5)")
        let image4 = SKTexture(imageNamed: "Jump (9)")
        let image5 = SKTexture(imageNamed: "RunShoot (2)")
               let image6 = SKTexture(imageNamed: "RunShoot (3)")
               let image7 = SKTexture(imageNamed: "RunShoot (5)")
               let image8 = SKTexture(imageNamed: "RunShoot (6)")
        let punchTextures = [image1,image2,image3,image4,image5,image6,image7,image8]
        let punchAnimation = SKAction.animate(
                       with: punchTextures,
                       timePerFrame: 0.3)
        self.enemy.run(punchAnimation)
        enemy.zPosition = 1
        
        let Action = SKAction.moveBy(x: 150, y:300, duration:1.0)
        let Action1 = SKAction.moveBy(x: 150, y:-300, duration:1.0)
        let Action2 = SKAction.moveBy(x: -300, y:0, duration:1.0)
        let jumpSequence = SKAction.sequence([Action, Action1, Action2])
        enemy.run(jumpSequence)
        //enemyRunShootAnimation()
        
    }
      func enemyDeadAnimation(){
        sound(name: "death")
            catdeadpause = true
            let image1 = SKTexture(imageNamed: "Dead (1)")
            let image2 = SKTexture(imageNamed: "Dead (3)")
            let image3 = SKTexture(imageNamed: "Dead (5)")
            let image4 = SKTexture(imageNamed: "Dead (8)")
            let punchTextures = [image1,image2,image3,image4]
            let punchAnimation = SKAction.animate(
                           with: punchTextures,
                           timePerFrame: 0.2)
            self.enemy.run(punchAnimation)
            enemy.zPosition = 1
            
        }
    func spawnBulletEnemy() {
              
           //adding the bullet
              let bullet = SKSpriteNode(imageNamed: "ebullet")
           bullet.anchorPoint = CGPoint(x: 0.5,y: 0.5)
         bullet.position.x = self.enemy.position.x
         bullet.position.y = self.enemy.position.y
           bullet.size = CGSize(width: 67, height: 60)
           bullet.texture = SKTexture(imageNamed: "ebullet.png")
         bullet.xScale = -1
          
           bullet.zPosition = 999

     
       
               bullet.physicsBody = SKPhysicsBody(rectangleOf: bullet.size)
               bullet.physicsBody?.affectedByGravity = false
               bullet.physicsBody?.categoryBitMask = 8
               bullet.physicsBody?.collisionBitMask = 0
               bullet.physicsBody?.contactTestBitMask = 4
          
           
           addChild(bullet)
         self.enemyBulletArray.append(bullet)
           
              
         let move1 = SKAction.move(to: CGPoint(x: -40, y: self.enemy.position.y),
                                             duration: 3)
             
                   let stoneAnimation = SKAction.sequence(
                       [move1]
                   )
                 
                   bullet.run(stoneAnimation)
      
          }
    
    func showResultLabel(){
                 // showing the scores
                 var resultLabel : SKLabelNode?
          
                 
                 let background = SKSpriteNode(imageNamed: "bg")
                 background.position = CGPoint(x: frame.size.width / 2, y:frame.size.height / 2)
                                   background.size = CGSize(width: frame.width, height: frame.height)
                                   background.anchorPoint = CGPoint.zero
                                   background.position = CGPoint(x: 0, y: 70)
                                   background.zPosition = 2
                                   self.addChild(background)
        
      
                 //score labels
                 resultLabel = SKLabelNode(text: "Match Result")
                    resultLabel!.position = CGPoint(x:700, y:390)
                    resultLabel!.fontColor = UIColor.magenta
                    resultLabel!.fontSize = 100
                    resultLabel!.fontName = "Avenir"
                 resultLabel!.zPosition = 3

                    addChild(resultLabel!)
    
      
        if(self.enemyLives > 0){
                   resultLabel?.text = "Match Result: You Lost"
               }
        else{
                  resultLabel?.text = "Match Result: You Won"
              }
                   
              
             }
    }

