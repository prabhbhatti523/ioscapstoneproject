//
//  MultiGameViewController.swift
//  capstoneGame
//
//  Created by Satinder pal Singh on 2019-12-06.
//  Copyright © 2019 Prabhjinder. All rights reserved.
//

import Foundation
import UIKit
import SpriteKit
import GameplayKit

class MultiGameViewController: UIViewController, MultiTransitionDelegate{
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let view = self.view as! SKView? {
            // Load the SKScene from 'GameScene.sks'
            
            
            if let scene = SKScene(fileNamed: "MultiGameScene") {
                // Set the scale mode to scale to fit the window
                scene.scaleMode = .aspectFill
                scene.delegate = self as! MultiTransitionDelegate
                
                // Present the scene
                view.presentScene(scene)
            }
            
            view.ignoresSiblingOrder = true
            //view.showsPhysics = true
            
            view.showsFPS = true
            view.showsNodeCount = true
            
            
        }
    }
    func returnToMainMenu(){
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        guard  let storyboard = appDelegate.window?.rootViewController?.storyboard else { return }
        if let vc = storyboard.instantiateInitialViewController() {
            print("go to main menu")
            self.present(vc, animated: true, completion: nil)
        }
    }

    override var shouldAutorotate: Bool {
        return true
    }

    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        if UIDevice.current.userInterfaceIdiom == .phone {
            return .allButUpsideDown
        } else {
            return .all
        }
    }

    override var prefersStatusBarHidden: Bool {
        return true
    }
}
